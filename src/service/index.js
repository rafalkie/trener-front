import axios from "axios";
import { url } from "../App";

export const getService = (type) => {
  return axios.get(`${url}/${type}`).then(response => response.data.data)
};
export const getServiceNonePaginate = (type) => {
  return axios.get(`${url}/${type}`).then(response => response.data)
};
