import React, { useEffect } from "react";
import { useCountUp } from "react-countup";

function Cirlcle(props) {
    const {end,run} = props;
  const { countUp, start } = useCountUp({
    start: 0,
    end,
    delay: 2500,
    duration: 10
  });
  useEffect(() => {
      if(run){
    start();}
  }, [run]);
  return (
    <div className="performance__item">
      <p>{countUp}</p>
    </div>
  );
}

export default Cirlcle;
