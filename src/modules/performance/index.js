import React, { useEffect, useState, useRef } from "react";
import Cirlcle from "./Cirlcle";
import performance from "../../img/performance.jpg";
import {useSelector} from "react-redux";
import { url } from "../../App";

function Performance() {
  const [isSticky, setSticky] = useState(false);
  const data = useSelector(state => state.front.achievements);
  const photo = useSelector(state => state.front.others);
  const {
    image_performance
  } = photo;
  const ref = useRef(null);
  const handleScroll = () => {
    setSticky(ref.current &&(ref.current.getBoundingClientRect().bottom <= 0 || ref.current.getBoundingClientRect().bottom >= 0));
    handleSearch();
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", () => handleScroll);
    };
  }, []);
  const handleSearch = () => {
    window.removeEventListener("scroll", handleScroll);
  };
  useEffect(() => {
    if (isSticky) {
      handleSearch();
    }
  }, [isSticky]);

  return (
    <div id="section-2" className="performance__border">
      <div
        className="performance"
        style={{
          backgroundImage: `url("${url}/uploads/${image_performance &&
            image_performance.folder}/${image_performance && image_performance.name}")`
        }}
        ref={ref}
      >
        <div className="container performance__container">
          {
            data.map((item,index)=>
              <div key={index}>
              <Cirlcle end={item.number} run={isSticky} />
              <h3>{item.name}</h3>
            </div>
          )
          }
         
          
        </div>
      </div>
    </div>
  );
}

export default Performance;
