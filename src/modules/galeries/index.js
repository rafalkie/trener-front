import React, { Component } from 'react';
import Carousel, { Modal, ModalGateway } from 'react-images';
import { getServiceNonePaginate } from '../../service';
import { url } from '../../App';
import axios from "axios";
import Loader from 'react-loader-spinner'



class Galeries extends Component {
    _isMounted = false;
    state = {
        limit: 8,
        total: 0,
        modalIsOpen: false,
        lightbox: [],
        galleries: [],
        isLoading: true,
        isLoadingMore: false,
        modalLoading: false
    };
    componentDidMount() {
        const { limit } = this.state;
        this._isMounted = true;
        getServiceNonePaginate(`galleries?$limit=${limit}`).then(data => {
            if (this._isMounted) {
                this.setState({
                    galleries: data.data,
                    total: data.total,
                    isLoading: false
                })
            }
        })
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    toggleModal = () => {
        this.setState(state => ({ modalIsOpen: !state.modalIsOpen }));
    };
    start = async (img) => {
        const urlGallery = `galleries/${img}`;
        this.setState({
            modalLoading: true,
        });
        await axios.get(`${url}/${urlGallery}`).then(response => response.data).then(data => {
            const getSrc = data.gallery.map(item => ({ src: `${url}/uploads/${item.folder}/${item.name}` }))
            return (
                this.setState({
                    lightbox: getSrc,
                    modalIsOpen: true,
                    modalLoading: false
                }))
        })
    }
    handleLoadMore = () => {
        let { limit, total, galleries } = this.state;
        let how_much = 8;
        let limitLocal;
        this.setState({ isLoadingMore: true })
        if (limit < total) {
            limitLocal = limit + how_much
            getServiceNonePaginate(`galleries?$skip=${limit}&$limit=${how_much}`).then(data => {

                this.setState({
                    limit: limitLocal,
                    galleries: [...galleries, ...data.data],
                    isLoadingMore: false
                })
            })
        }

    }
    render() {
        const { modalIsOpen, lightbox, galleries, isLoading, limit, total, isLoadingMore, modalLoading } = this.state;
        return (
            <div id="section-3" className="galeries">
                <div className="wraper">
                    <h1 className="title">Galeria</h1>
                    <div className="galeries__flex">
                        {
                            !isLoading && galleries.map((item, index) => {
                                if (item.gallery[0] !== undefined) {
                                    const { folder, name } = item.gallery[0];
                                    return (
                                        <img key={index} src={`${url}/uploads/${folder}/${name}`} alt="" onClick={() => this.start(item._id)} />
                                    )
                                } else return null
                            }
                            )

                        }
                    </div>
                    <Loader className={modalLoading ? 'galeries__loadModal': 'galeries__loadModal galeries__loadModal-none' }
                        type="ThreeDots"
                        color="white"
                    />
                    {isLoadingMore && <Loader
                        type="ThreeDots"
                        color="#bc9655"
                    />
                    }
                    {!isLoadingMore && (limit < total && <button className="galeries__loadMore" onClick={this.handleLoadMore}>Załaduj więcej...</button>)}
                    <ModalGateway>
                        {modalIsOpen && (
                            <Modal onClose={this.toggleModal}>
                                <Carousel views={lightbox} />
                            </Modal>
                        )}
                    </ModalGateway>
                </div>
            </div>
        )
    }
}

export default Galeries;
