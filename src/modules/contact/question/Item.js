import React, { useState } from "react";

function Item(props) {
  const { answer, question } = props;
  const [open, setOpen] = useState(false);
  return (
    <div>
      <h4 onClick={() => setOpen(!open)}>{question} <span className={open ? "arrow arrow--off" : "arrow arrow--on"}></span></h4>
     
      <p className={open ? "question__item question__item--open" : "question__item"}>
        {answer}
      </p>
    </div>
  );
}

export default Item;
