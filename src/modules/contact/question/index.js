import React from "react";
import Item from "./Item";
import { useSelector } from "react-redux";

function Question() {
  const data = useSelector(state => state.front.faq);

  return (
    <div className="question">
      <h2>Najczęściej zadawane pytania</h2>
      <div className="question__container">
        {data.map((item, index) => (
          <Item key={index} answer={item.answer} question={item.question} />
        ))}
      </div>
    </div>
  );
}

export default Question;
