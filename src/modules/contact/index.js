import React from 'react'
import Form from './form';
import Question from './question';

const Contact = () => {
    return (
        <div className="contact">
            <div className="container">
                <h1 className="title">Masz pytanie ?</h1>
                <div className="contact__container">
                    <Form  />
                    <Question />
                </div>
            </div>
        </div>
    )
}

export default Contact;