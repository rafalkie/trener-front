import React, { useState } from "react";
import axios from "axios";
import ReCAPTCHA from "react-google-recaptcha";
import { url } from "../../App";
import { useSelector } from "react-redux";

function Form() {
  const [formData, setFormData] = useState({
    info: [1, ""],
    user: "",
    from: "",
    subject: "",
    message: "",
    isVerified: false,
    visibleCaptcha: false
  });
  const data = useSelector(state => state.front.others);
  const {captcha_gmail } = data;
  const send = async () => {
    const {
      user,
      from,
      subject,
      message,
      isVerified,
      visibleCaptcha
    } = formData;
    const { footer_email } = data;

    if (isVerified) {
      await axios
        .post(`${url}/send-email`, { user, from, subject, message, to:footer_email })
        .then(() => {
          setFormData({
            info: [1, "Wiadomość wysłana"]
          });
        })
        .catch(() => {
          const formDataCopy = formData;

          setFormData({...formDataCopy,
            info: [0, "Wiadomość nie wysłana"]
          });
        });

      setTimeout(() => {
        setFormData({    user: "",
        from: "",
        subject: "",
        message: "",
        visibleCaptcha: false,
          info: [1, ""]
        });
      }, 2500);
    } else {
      const formDataCopy = formData;

      setFormData({...formDataCopy,
        visibleCaptcha: true
      });
      if (visibleCaptcha) alert("Potwierdź że nie jesteś robotem!");
    }
  };
  const onChangeValue = (event, type) => {
    const formDataCopy = formData;
    setFormData({...formDataCopy,
      [type]: event.target.value
    });
  };

  const onChange = () => {
    const formDataCopy = formData;
    setFormData({...formDataCopy,
      isVerified: true
    });
  };
  const onExpired = () => {
    const formDataCopy = formData;
    setFormData({...formDataCopy,
      isVerified: false
    });
  };
  const { user, from, subject, message, visibleCaptcha, info } = formData;

  return (
    <div id="section-5"  className="contact__form">
      <h2>Formularz kontaktowy</h2>
      <div className="contact__flex">
        <form
          onSubmit={e => {
            send();
            e.preventDefault();
          }}
        >
          <p>Imie i nazwisko</p>
          <input
            type="text"
            name="user"
            value={user}
            onChange={event => onChangeValue(event, "user")}
            required
          />
          <p>E-mail</p>
          <input
            type="email"
            name="from"
            value={from}
            onChange={event => onChangeValue(event, "from")}
            required
          />
           <p>Treść wiadomości</p>
          <input
            type="text"
            name="subject"
            value={subject}
            onChange={event => onChangeValue(event, "subject")}
            required
          />
              <p>Temat</p>
          <textarea
            type="input"
            name="message"
            value={message}
            onChange={event => onChangeValue(event, "message")}
            required
          />
          {visibleCaptcha && (
            <ReCAPTCHA
              sitekey={captcha_gmail}
              onChange={onChange}
              onExpired={onExpired}
              className="captcha"
            />
          )}
          <button type="submit" className="button">
            {" "}
            Wyślij{" "}
          </button>
          <p
            className={
              info[0] === 1 ? "contact__send--sucess" : "contact__send--fail"
            }
          >
            {info[1]}
          </p>
        </form>
      </div>
    </div>
  );
}

export default Form;
