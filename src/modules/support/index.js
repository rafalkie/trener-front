import React, { useEffect } from "react";
import { getService } from "../../service";
import Image from "../../component/Image";
import {useSelector} from "react-redux";

function Support() {
  const data = useSelector(state => state.front.support);
  return (
    <div id="section-4" className="support">
      <div className="container support__container">
        {data &&
          data.map((item, index) => (
            <div key={index} className="support__item">
              <div>
                <h3>{item.title}</h3>
                <p>{item.description}</p>
              </div>
              <Image item={item.image} alt="ikona" />
            </div>
          ))}
      </div>
    </div>
  );
}

export default Support;
