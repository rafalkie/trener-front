import React from "react";
import { NavLink, Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import AnchorLink from "react-anchor-link-smooth-scroll";
import { useSelector } from "react-redux";

const useStyles = makeStyles({
  list: {
    width: 250,
    padding: 15
  }
});

export default function Menu(props) {
  const page = useSelector(state => state.front.page);
  const classes = useStyles();
  const [state, setState] = React.useState({
    left: false
  });
  let menuItem = [{ name: "Strona główna", link: "/" }];
  page.forEach(item => {
    if (item.visible) {
      if (item.module !== "page") {
        menuItem.push({ name: `${item.name}`, link:item.module === "aktualnosci" ? `/${item.module}/1` : `/${item.module}/` });
      } else {
        menuItem.push({ name: `${item.name}`, link: `/${item.name}` });
      }
    }
  });
  const toggleDrawer = (side, open) => event => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setState({ ...state, [side]: open });
  };
  const sideList = side => (
    <div
      className={classes.list}
      role="presentation"
      onClick={toggleDrawer(side, false)}
      onKeyDown={toggleDrawer(side, false)}
    >
      <List>
        {menuItem.map((item, index) => (
          <Link to={item.link} key={index}>
            <ListItem button>
              <ListItemText primary={item.name} />
            </ListItem>
          </Link>
        ))}
        <Link to={`#section-5`}>
          <AnchorLink href={`#section-6`}>
            <ListItem button>
              <ListItemText primary="Kontakt" />
            </ListItem>
          </AnchorLink>
        </Link>
      </List>
    </div>
  );

  return (
    <div>
      {!state.left ? (
        <div className="button__menu" onClick={toggleDrawer("left", true)}>
          <span></span>
        </div>
      ) : null}
      <div className="menu">
        {menuItem.map((item, index) => (
          <NavLink
            key={index}
            exact={true}
            to={item.link}
            activeClassName={"is-active"}
          >
            {item.name}
          </NavLink>
          
        ))}
        <AnchorLink href={`#section-6`}>
             Kontakt
           </AnchorLink>
      </div>
      <Drawer open={state.left} onClose={toggleDrawer("left", false)}>
        {sideList("left")}
      </Drawer>
    </div>
  );
}
