import React from "react";
import Menu from "./menu";
import ScrollBar from "./scrollBar";
import { Link, withRouter } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { url } from "../../App";
import { loadImage } from "../../store/action";
import Image from "../../component/Image";
import pageTransition from "./../../component/PageTransition";
import { motion } from "framer-motion";

function FirstPage(props) {
	const dispatch = useDispatch();
	const data = useSelector((state) => state.front.others);
	const { first_section_subtitle, first_section_title, image_first, logo } = data;

	return (
		<div id="section-0" className="firstPage">
			<div className="container">
				<div className="firstPage__top">
					<Link to="/" className="firstPage__logo">
						<Image item={logo} alt="logo" width="auto" onLoad={() => dispatch(loadImage())} />
					</Link>
					<Menu />
				</div>
				<motion.div initial="out" exit="out" animate="in" variants={pageTransition}>

				<div className="firstPage__container">
				<div className="firstPage__left">
						<div className="firstPage__text">
							<h1>{first_section_title}</h1>
							<h3>{first_section_subtitle}</h3>
						</div>
						<div className="firstPage__button">
							<button onClick={() => props.history.push("/oferta")} className="button__primary">
								SPRAWDŹ OFERTE
							</button>
							<button onClick={() => props.history.push("/o-mnie")} className="button__secondary">
								KIM JESTEM
							</button>
						</div>
					</div>

					<div
						style={{
							backgroundImage: `url("${url}/uploads/${image_first &&
								image_first.folder}/${image_first && image_first.name}")`
						}}
						className="firstPage__right"
					></div>

				
				
				</div>
				</motion.div>
				<div className="scrollBar">
						<ScrollBar />
					</div>
			</div>
		</div>
	);
}

export default withRouter(FirstPage);
