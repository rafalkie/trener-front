import React from "react";
import { useSelector,useDispatch } from "react-redux";
import Image from "../../component/Image";
import { url } from "../../App";
import { loadImage } from "../../store/action";

function AboutMe() {
  const data = useSelector(state => state.front.others);
  const { about_me_title, about_me_subtitle, about_me_description } = data;
  const dispatch = useDispatch();
  const style = {
    backgroundImage: `url("${url}/uploads/${data.image_second &&
      data.image_second.folder}/${data.image_second &&
      data.image_second.name}")`,
    backgroundPositionY: "bottom",
    backgroundPositionX: "-200px",
    backgroundRepeat: "no-repeat",
    opacity: "0.1",
    height: "inherit",
    position: "absolute",
    top: 0,
    bottom: 0,
    margin: "auto",
    left: 0,
    right: 0,
    width: "52%"
  };
  return (
    <div id="section-1" className="container">
      <div className="aboutMe__background" style={style}></div>
      <div className="aboutMe">
        <div className="aboutMe__img">
          <Image item={data.image_second} alt="Adrian Cios" onLoad={()=>dispatch(loadImage())} />
        </div>
        <div className="aboutMe__text">
          <h1>{about_me_title}</h1>
          <h3>{about_me_subtitle}</h3>
          <p>{about_me_description}</p>
        </div>
      </div>
    </div>
  );
}

export default AboutMe;
