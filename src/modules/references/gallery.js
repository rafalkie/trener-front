import React from "react";
import AliceCarousel from "react-alice-carousel";
import "react-alice-carousel/lib/alice-carousel.css";
import avatar from "./../../img/avatar.jpg";
import { url } from "../../App";
import CheckedShort from "./checkedShort";
import Modal from "react-responsive-modal";

class Gallery extends React.Component {
  state = {
    visible: false,
    currentIndex: 0,
    currentIndex2: 0,
    open: false,
    selectedID: 0,
    responsive: {
      0: { items: 1 },
      850: { items: 2 },
      1024: { items: 3 }
    },
    galleryEven: this.galleryItems("even"),
    galleryOdd: this.galleryItems("odd")
  };
  onOpenModal = index => {
    this.setState({ visible: true, selectedID: index });
  };
  closeModal = () => {
    this.setState({ visible: false });
  };
  slideTo = (i, index) => this.setState({ [index]: i });
  slideNext = index => this.setState({ [index]: this.state[index] + 1 });
  slidePrev = index => this.setState({ [index]: this.state[index] - 1 });
  onSlideChanged = (e, index) => this.setState({ [index]: e.item });

  galleryItems(type) {
    const { references } = this.props;
    const odd = references.filter((item, index) =>
      index % 2 === 0 ? item : null
    );
    const even = references.filter((item, index) =>
      index % 2 === 1 ? item : null
    );
    let what = type === "odd" ? odd : even;

    return what.map((item, index) => (
      <div key={index} className="references__gallery">
        <CheckedShort
          onOpenModal={item => this.onOpenModal(item)}
          item={item}
          index={index}
        />
        <div className="references__gallery__author">
          {item.image !== undefined ? (
            <img
              src={`${url}/uploads/${item.image.folder}/${item.image.name}`}
              alt=""
              width="250px"
            />
          ) : (
            <img src={avatar} alt="" width="250px" />
          )}
          <div>
            <p className="references__gallery__author--name">{item.name}</p>
          </div>
        </div>
      </div>
    ));
  }

  render() {
    const { selectedID, visible } = this.state;
    const { references } = this.props;
    const {
      currentIndex,
      currentIndex2,
      responsive,
      galleryEven,
      galleryOdd
    } = this.state;
    const modalFilter = references.filter(item => item._id === selectedID);
    return (
      <div className="references__container">
        <div className="references__container--one">
          <AliceCarousel
            mouseDragEnabled 
            dotsDisabled={true}
            buttonsDisabled={true}
            items={galleryOdd}
            responsive={responsive}
            slideToIndex={currentIndex}
            onSlideChanged={e => this.onSlideChanged(e, "currentIndex")}
          />
        </div>

        <div className="references__nav">
          <div
            className="references__nav__btn references__nav__btn--left"
            onClick={() => {
              this.slidePrev("currentIndex");
              this.slidePrev("currentIndex2");
            }}
          >
            <span className="icon-ctrl-left"></span>
          </div>
          <div className="references__nav__line"></div>
          <div
            className="references__nav__btn refrences__nav__btn--right"
            onClick={() => {
              this.slideNext("currentIndex2");
              this.slideNext("currentIndex");
            }}
          >
            <span className="icon-ctrl-right"></span>
          </div>
        </div>

        <div className="references__container--two">
          <AliceCarousel
            mouseDragEnabled
            dotsDisabled={true}
            buttonsDisabled={true}
            items={galleryEven}
            responsive={responsive}
            slideToIndex={currentIndex2}
            onSlideChanged={e => this.onSlideChanged(e, "currentIndex2")}
          />
        </div>

        <Modal
          open={visible}
          onClose={() => this.setState({ visible: false })}
          center
        >
          <div className="modal-responsive">
            {modalFilter[0] && (
              <div
                dangerouslySetInnerHTML={{ __html: modalFilter[0].description }}
              />
            )}
          </div>
        </Modal>
      </div>
    );
  }
}

export default Gallery;
