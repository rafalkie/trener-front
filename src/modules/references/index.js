import React from "react";
import Gallery from "./gallery";
import {useSelector} from "react-redux";

function Referencies() {
  const data = useSelector(state => state.front.references);
  return (
    <div id="section-3" className="references">
      <div className="wraper container">
        <h1 className="title">Referencje</h1>
        <Gallery references={data} />
      </div>
    </div>
  );
}

export default Referencies;
