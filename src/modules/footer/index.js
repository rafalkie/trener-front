import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Link, NavLink } from "react-router-dom";
import Image from "../../component/Image";

const Footer = () => {
  const data = useSelector(state => state.front.others);
  const {
    footer_description,
    footer_email,
    footer_fb,
    footer_tel,
    logo,
    footer_instagram
  } = data;
  return (
    <div id="section-6" className="footer">
      <div className="container">
        <div className="footer__top">
          <div className="footer__logo">
            <Image item={logo} alt="logo" />
          </div>
          <div className="footer__column">
            <h3>Oferta</h3>
            <p>
              <Link to="/oferta">Pakiety</Link>
            </p>
          </div>
          <div className="footer__column">
            <h3>Szybkie linki</h3>
          
            <p>
              <Link to="/regulamin" replace>
                Regulamin
              </Link>
            </p>
            <p>
              <Link to="/aktualnosci/1" replace>
                Aktualności
              </Link>
            </p>
            <p>
              <Link to="/galeria">Galeria</Link>
            </p>
            <p>
              <Link to="o-mnie">Kim jestem ?</Link>
            </p>
          </div>
          <div className="footer__column">
            <h3>Dane kontaktowe</h3>
            <p>
              <span className="icon-envelop"></span>
              <a href={`mailto:${footer_email}`}> {footer_email}</a>
            </p>
            <p>
              <span className="icon-mobile"></span>
              <a href={`tel: ${footer_tel}`}> {footer_tel}</a>
            </p>
          </div>
        </div>
        <div className="footer__fb">
          <a href={`${footer_fb}`} target="_blank">
            <span className="icon-facebook2"></span> <p>Facebook</p>
          </a>
          <a href={`${footer_instagram}`} target="_blank">
            <span className="icon-instagram"></span> <p>Instagram</p>
          </a>
        </div>
        <div className="footer__bottom">{footer_description}</div>
      </div>
    </div>
  );
};

export default Footer;
