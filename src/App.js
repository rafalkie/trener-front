import React, { useEffect, Fragment } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./style/index.scss";
import Front from "./pages/front";
import PanelTopAdmin, { token } from "./component/panelTopAdmin";
import NoMatch from "./pages/404";

import { useDispatch, useSelector } from "react-redux";
import { getData } from "./store/action";
import Footer from "./modules/footer";
import Loader from "./component/Loader";
import News from "./pages/news";
import Offer from "./pages/offer";
import Gallery from "./pages/gallery";
import AboutMe from "./pages/aboutMe";
import Messenger from "./component/Messenger";
import NewsPage from "./pages/news/NewsPage";
import Forms from "./pages/forms";
import { AnimatePresence, motion } from "framer-motion";
import Regulations from "./pages/regulations";

export const url = process.env.REACT_APP_API;
export const domain = process.env.REACT_APP_DOMAIN;

function App() {
	const dispatch = useDispatch();
	const isLoading = useSelector((state) => state.front.loading);
	const isLoadingImage = useSelector((state) => state.front.loadingImage);
	const page = useSelector((state) => state.front.page);
	const others = useSelector((state) => state.front.others);

	const messenger_id = others && others.messenger_id;

	useEffect(() => {
		dispatch(getData("others"));
		dispatch(getData("faq", "?$sort[position]=1"));
		dispatch(getData("achievements", "?$limit=4&$sort[position]=1"));
		dispatch(getData("references", "?$limit=20&$sort[position]=1"));
		dispatch(getData("support", "?$sort[position]=1"));
		dispatch(getData("page", "?$sort[position]=1"));
		dispatch(getData("offer", "?$sort[position]=1"));
		dispatch(getData("galleries", "?$sort[position]=1"));
	}, []);
	useEffect(() => {
		window.title = others.about_me_description;
	}, [others]);
	if (isLoading < 8 && isLoadingImage !== 2) {
		return <Loader />;
	}
	const checkedIsVisible = (name) => {
		if (page !== undefined) {
			const data = page.filter((x) => x.module === name)[0];
			return data && data.visible;
		}
	};
	return (
		<Router>
			<div className={token() === null ? "app" : "app--admin"}>
				<Messenger pageId={messenger_id} version="5.0" />
				<Route component={ScrollToTop} />
				<AnimatePresence>
					<Switch>
						<Route path="/" exact component={Front} />
						{checkedIsVisible("aktualnosci") && (
							<Route exact path="/aktualnosci/:perPage" component={News} />
						)}
						{checkedIsVisible("aktualnosci") && (
							<Route exact path="/aktualnosci/:perPage/:title" component={NewsPage} />
						)}

						{checkedIsVisible("oferta") && <Route exact path="/oferta" component={Offer} />}
						{checkedIsVisible("galeria") && <Route exact path="/galeria" component={Gallery} />}
						{checkedIsVisible("o-mnie") && <Route exact path="/o-mnie" component={AboutMe} />}
						<Route exact path="/regulamin" component={Regulations} />
						<Route exact path="/forms/:id" component={Forms} />
						<Route component={NoMatch} />
					</Switch>
				</AnimatePresence>

				<PanelTopAdmin />
				<Footer />
			</div>
		</Router>
	);
}
const ScrollToTop = (props) => {
	const { pathname } = props.location;
	// Wszystkie elementy oprócz
	const pathNameTop = [""];
	if (!pathNameTop.includes(pathname)) {
		window.scrollTo(0, 0);
	}
	return null;
};

export default App;
