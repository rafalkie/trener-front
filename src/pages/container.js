import React from "react";
import { Link } from "react-router-dom";
import Menu from "../modules/first_page/menu";
import { useSelector } from "react-redux";
import Image from "../component/Image";
import pageTransition from "./../component/PageTransition";

import { motion } from "framer-motion";

const Container = (containerProps) => (Component) => (props) => {
	function ContainerF(props) {
		const data = useSelector((state) => state.front.others);
		const url = process.env.REACT_APP_API;
		const { logo } = data;
		const type = props.containerProps !== undefined ? props.containerProps.type : "";
		const page = useSelector((state) => state.front.page);
		const pageData = page.filter((x) => x.module === type)[0];

		let subtitle = pageData && pageData.subtitle;
		let title = pageData && pageData.name;
		let image = pageData && pageData.image;
		let image_id = pageData && pageData.image_id;

		const background =
			image_id !== undefined && image_id !== null
				? {
						backgroundImage: `url("${url}/uploads/${image && image.folder}/${image && image.name}")`
				  }
				: null;
		return (
			<>
				<div className="pages__navbar" style={{ ...background }}>
					<div className="container">
						<div className="firstPage__top">
							<Link to="/" className="firstPage__logo">
								<Image item={logo} alt="logo" width="170px" />
							</Link>
							<Menu />
						</div>
						<div className="pages__title ">
							<h1>{title}</h1>
							<h3>{subtitle}</h3>
						</div>
					</div>
				</div>
			</>
		);
	}

	return (
		<div className="pages__container ">
			<ContainerF containerProps={containerProps} />
			<>
				<motion.div initial="out" exit="out" animate="in" variants={pageTransition}>
					<Component />
				</motion.div>
			</>
		</div>
	);
};

export default Container;
