import React from "react";

function NoMatchWithoutContainer() {
  return (
    <div className="noMatch container">
      <div>
        <h2>Nie znaleziono</h2>
        <h2>strony o podanym adresie</h2>
      </div>
    </div>
  );
}

export default NoMatchWithoutContainer;
