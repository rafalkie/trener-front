import React from "react";
import Container from "../container";

function NoMatch() {
  return (
    <div className="noMatch container">
        <div>
      <h2>Nie znaleziono</h2>
      <h2>strony o podanym adresie</h2>
      </div>
    </div>
  );
}

export default Container()(NoMatch);
