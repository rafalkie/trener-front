import React, { useEffect, useState } from "react";
import axios from "axios";
import ReCAPTCHA from "react-google-recaptcha";
import { url } from "../../App";
import { useSelector } from "react-redux";
import TextareaAutosize from "react-textarea-autosize";
import { withRouter } from "react-router-dom";

function Form(props) {
  const { offer, questions } = props;
  const [answer, setAnswer] = useState([]);
  const [formData, setFormData] = useState({
    info: [1, ""],
    user: "",
    from: "",
    isVerified: false,
    visibleCaptcha: false
  });
  const { hash } = props.location;
  const data = useSelector(state => state.front.others);
  const { captcha_gmail, forms_thx, forms_title } = data;
  const send = async () => {
    const { user, from, questions, isVerified, visibleCaptcha } = formData;
    const { footer_email } = data;
    let message = `<p style={{"text-align":"center"}}>Dla pakietów: ${offer.map(
      item => item.package_name
    )}</p>`;
    answer.forEach(
      (item, index) =>
        (message += `	<div>
				<h3>Pytanie ${index + 1}. ${item.question}</h3>
				<p>Odpwiedź: ${item.answer}</p>
			</div>`)
    );
    if (isVerified) {
      await axios
        .post(`${url}/send-email`, {
          user,
          from,
          subject: `Ankieta klienta - ${user}`,
          message,
          to: footer_email
        })
        .then(() => {
          setFormData({
            info: [1, "Wiadomość wysłana"]
          });
        })
        .catch(() => {
          const formDataCopy = formData;

          setFormData({ ...formDataCopy, info: [0, "Wiadomość nie wysłana"] });
        });

      setTimeout(() => {
        setFormData({
          user: "",
          from: "",
          message: "",
          visibleCaptcha: false,
          info: [1, ""]
        });
      }, 2500);
    } else {
      const formDataCopy = formData;

      setFormData({ ...formDataCopy, visibleCaptcha: true });
      if (visibleCaptcha) alert("Potwierdź że nie jesteś robotem!");
    }
  };
  const onChangeValue = (event, question, index) => {
    let answerCopy = answer;
    answerCopy[index] = { answer: event.target.value, question };
    setAnswer([...answerCopy]);
  };
  const onChangeFormData = (event, type) => {
    const formDataCopy = formData;
    setFormData({ ...formDataCopy, [type]: event.target.value });
  };
  const onChange = () => {
    const formDataCopy = formData;
    setFormData({ ...formDataCopy, isVerified: true });
  };
  const onExpired = () => {
    const formDataCopy = formData;
    setFormData({ ...formDataCopy, isVerified: false });
  };
  return (
    <div className="contact__form">
      {hash === "#finish" ? (
        <div dangerouslySetInnerHTML={{ __html: forms_thx }}></div>
      ) : (
        <div dangerouslySetInnerHTML={{ __html: forms_title }}></div>
      )}
      <p style={{ "text-align": "center" }}>
        [{" "}
        {offer.map(
          (item, index) =>
            `${item.package_name} ${offer.length !== index + 1 ? `, ` : ``}`
        )}
        ]
      </p>
      <div className="contact__flex">
        <form
          onSubmit={e => {
            send();
            e.preventDefault();
          }}
        >
          <p>Imie i nazwisko</p>
          <input
            type="text"
            name="user"
            value={formData.user}
            onChange={event => onChangeFormData(event, "user")}
            required
          />
          <p>E-mail</p>
          <input
            type="email"
            name="from"
            value={formData.from}
            onChange={event => onChangeFormData(event, "from")}
            required
          />
          {questions.map((item, index) => (
            <div className="forms__box" key={index}>
              <p>{item.question}</p>
              <TextareaAutosize
                className="text__area"
                minRows="2"
                type="input"
                name={index}
                value={(answer[index] && answer[index].answer) || ""}
                onChange={event => onChangeValue(event, item.question, index)}
                required
              />
            </div>
          ))}
          {formData.visibleCaptcha && (
            <ReCAPTCHA
              sitekey={captcha_gmail}
              onChange={onChange}
              onExpired={onExpired}
              className="captcha"
            />
          )}
          <button type="submit" className="button">
            Wyślij
          </button>
          <p
            className={
              formData.info[0] === 1
                ? "contact__send--sucess"
                : "contact__send--fail"
            }
          >
            {formData.info[1]}
          </p>
        </form>
      </div>
    </div>
  );
}

export default withRouter(Form);
