import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import Image from "../../component/Image";
import Menu from "../../modules/first_page/menu";
import { getData } from "../../store/action";
import { getService } from "../../store/services";
import Form from "./Form";
import pageTransition from "./../../component/PageTransition";
import { motion } from "framer-motion";
function Forms(props) {
	const others = useSelector((state) => state.front.others);
	const { logo } = others;
	const id = props.match.params.id;
	const [data, setData] = useState();

	useEffect(() => {
		getService(`forms?_id=${id}`).then((response) => setData(response.data.data));
	}, []);
	return (
		<div>
			<div className="pages__navbar pages__navbar--news">
				<div className="container">
					<div className="firstPage__top">
						<Link to="/" className="firstPage__logo">
							<Image item={logo} alt="logo" width="170px" />
						</Link>
						<Menu />
					</div>
				</div>
			</div>
			{data && (
				<motion.div initial="out" exit="out" animate="in" variants={pageTransition}>
					<div className="container forms__page">
						{data[0].questions && <Form questions={data[0].questions} offer={data[0].offer} />}
					</div>
				</motion.div>
			)}
		</div>
	);
}

export default Forms;
