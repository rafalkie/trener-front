import React, { Component } from "react";
import FirstPage from "../modules/first_page";
import Galeries from "../modules/galeries";
import References from "../modules/references";
import Contact from "../modules/contact";
import Performance from "./../modules/performance/index";
import AboutMe from "../modules/aboutMe";
import Support from "../modules/support";

class Front extends Component {
  render() {
    return (
      <>
        <FirstPage  />
        <AboutMe />
        <Performance />
        <References />
        <Support />
        {/* <Galeries /> */}
        <Contact />
      </>
    );
  }
}

export default Front;
