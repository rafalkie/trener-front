import React from "react";
import Container from "../container";
import { useSelector } from "react-redux";

function AboutMe() {
  const data = useSelector(state => state.front.page);
  return (
    <div className="container">
      <div className="aboutMePage pages__style">
        <div
          className="aboutMePage__description"
          dangerouslySetInnerHTML={{ __html: data[0].text }}
        ></div>
      </div>
    </div>
  );
}

export default Container({ type: "o-mnie" })(AboutMe);
