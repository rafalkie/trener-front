import React from "react";
import Container from "../container";
import { useSelector } from "react-redux";
import Image from "../../component/Image";
import { Link } from "react-router-dom";
import Menu from "../../modules/first_page/menu";
import pageTransition from "../../component/PageTransition";
import { motion } from "framer-motion";

function Regulations() {
  const data = useSelector(state => state.front.others);
  const others = useSelector(state => state.front.others);
  const { logo } = others;
  return (
    <>
      <div className="pages__navbar pages__navbar--news">
        <div className="container">
          <div className="firstPage__top">
            <Link to="/" className="firstPage__logo">
              <Image item={logo} alt="logo" width="170px" />
            </Link>
            <Menu />
          </div>
        </div>
      </div>
      <motion.div
        initial="out"
        exit="out"
        animate="in"
        variants={pageTransition}
      >
        <div className="container">
          <div className="aboutMePage pages__style">
            <div
              className="aboutMePage__description"
              dangerouslySetInnerHTML={{ __html: data.regulations }}
            ></div>
          </div>
        </div>
      </motion.div>
    </>
  );
}

export default Regulations;
