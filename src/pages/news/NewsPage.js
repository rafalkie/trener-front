import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import Image from "../../component/Image";
import Menu from "../../modules/first_page/menu";
import { getData } from "../../store/action";
import { getService } from "../../store/services";
import pageTransition from "./../../component/PageTransition";
import { motion } from "framer-motion";

function NewsPage(props) {
  const others = useSelector(state => state.front.others);
  const news = useSelector(state => state.front.news);

  const { logo } = others;
  const titleUrl = props.match.params.title;
  const { perPage } = props.match.params;
  const [filterNews,setFilterNews] = useState();
  const [data, setData] = useState();
  useEffect(() => {
    if (news === undefined || (news[perPage] === undefined) ) {
      getService(`news?titleUrl=${titleUrl}`).then(response =>
        setData(response.data.data)
      );
    }else{
      setFilterNews(news[perPage].filter(item => item.titleUrl === titleUrl )[0])
      
    }
  }, []);
  return (
    <div>
      <div className="pages__navbar pages__navbar--news">
        <div className="container">
          <div className="firstPage__top">
            <Link to="/" className="firstPage__logo">
              <Image item={logo} alt="logo" width="170px" />
            </Link>
            <Menu />
          </div>
        </div>
      </div>

      <motion.div
        initial="out"
        exit="out"
        animate="in"
        variants={pageTransition}
      >
        <div className="container">
      { (news === undefined || (news[perPage] === undefined) )?
        <div className="newsPage">
            {data && (
              <>
                <h3>{data[0] && data[0].title}</h3>
                <p className="newsPage__date">
                  {data[0] && data[0].createdAt.slice(0, 10)}
                </p>
                <div className="newsPage__img">
                  {data[0].image && <Image item={data[0].image} alt="logo" />}
                </div>
                <div
                  className="newsPage__text"
                  dangerouslySetInnerHTML={{ __html: data[0] && data[0].text }}
                ></div>
              </>
            )}
          </div>
        :
          <div className="newsPage">
            {filterNews && (
              <>
                <h3>{filterNews.title}</h3>
                <p className="newsPage__date">
                  {filterNews.createdAt.slice(0, 10)}
                </p>
                <div className="newsPage__img">
                  {<Image item={filterNews.image} alt="logo" />}
                </div>
                <div
                  className="newsPage__text"
                  dangerouslySetInnerHTML={{ __html:filterNews.text }}
                ></div>
              </>
            )}
          </div>}
        </div>
      </motion.div>
    </div>
  );
}

export default NewsPage;
