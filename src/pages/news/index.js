import React, { useEffect } from "react";
import Container from "../container";
import { useDispatch, useSelector } from "react-redux";
import { getData, changePage } from "../../store/action";
import { Link } from "react-router-dom";
import { url } from "../../App";
import Pagination from "react-js-pagination";
import { withRouter } from "react-router-dom";

function News(props) {
	const dispatch = useDispatch();
	const news = useSelector((state) => state.front.news);
	const pagination = useSelector((state) => state.front.pagination);
	const { active, perPage, total } = pagination;
	const activePageUrl = props.match.params.perPage;

	useEffect(() => {
		dispatch(changePage(parseInt(activePageUrl)));
		if (news === undefined) {
			const { perPage } = pagination;
			dispatch(
				getData(
					"news",
					`?$limit=${perPage}&$skip=${(activePageUrl - 1) * perPage}&$sort[createdAt]=-1`
				)
			);
		}
	}, []);
	useEffect(() => {
		if (news) {
			const { active, perPage } = pagination;
			if (news[active] === undefined) {
				dispatch(
					getData("news", `?$limit=${perPage}&$skip=${(active - 1) * perPage}&$sort[createdAt]=-1`)
				);
			}
		}
	}, [pagination.active]);
	return (
		<div className="container">
			<div className="news">
				{news &&
					news[active] &&
					news[active].map((item, index) => (
						<div key={index} className="news__box">
							<Link to={`/aktualnosci/${active}/${item.titleUrl}`}>
								<div
									className="news__thumbnail"
									style={{
										backgroundImage: `url("${url}/uploads/${item.image &&
											item.image.folder}/${item.image && item.image.name}")`
									}}
								>
									<p>{item.createdAt.slice(0, 10)}</p>
								</div>
								<h3>{item.title}</h3>
							</Link>
						</div>
					))}
			</div>
			{total > 6 && (
				<Pagination
					activePage={active}
					itemsCountPerPage={perPage}
					totalItemsCount={pagination.total}
					onChange={(value) => {
						dispatch(changePage(value));
						props.history.push(`/aktualnosci/${value}`);
					}}
				/>
			)}
		</div>
	);
}

export default Container({ type: "aktualnosci" })(withRouter(News));
