import React, { useState, useEffect } from "react";
import Container from "../container";
import Box from "./Box";
import { useSelector, useDispatch } from "react-redux";
import { getService } from "../../store/services";
import { getData } from "./../../store/action";

function Offer() {
  const dispatch = useDispatch();

  const data = useSelector(state => state.front.offer);
  const page = useSelector(state => state.front.page);
  const description = page.filter(x => x.module === "oferta")[0];
  useEffect(() => {
    dispatch(getData("forms"));
  }, []);
  return (
    <div className="offer">
      <div className="container">
      <h1 className="offer__title">Współpraca online</h1>
        <div
          className="offer__text"
          dangerouslySetInnerHTML={{ __html: description.text }}
        ></div>
        <div className="offer__online">
          {data &&
            data.map(
              (item, index) =>
                item.type === "online" && (
                  <Box className="offer__online-box" key={index} data={item} />
                )
            )}
        </div>
        <div className="offer__container">
          {data &&
            data.map(
              (item, index) =>
                item.type === "normal" && (
                  <Box className="offer__box" key={index} data={item} />
                )
            )}
        </div>
     
      </div>
    </div>
  );
}

export default Container({ type: "oferta" })(Offer);
