import React from "react";
import Form from "./Form";

function Box(props) {
	const { package_name, name, description, price, data, type } = props.data;
	const { className } = props;
	return (
		<>
			{type == "normal" ? (
				<div className={props.data.super === "normal" ? className  :  `${className} ${ className}-${props.data.super}`}>
					<div className="offer__package">{package_name}</div>
					<div className="offer__content">
						<div className="offer__name">{name}</div>
						<div
							className="offer__description"
							dangerouslySetInnerHTML={{ __html: description }}
						></div>
					</div>
					<div className="offer__container-button">
						<span className="offer__price">{price}</span>
						<span className="offer__currency">zł</span>
						<span className="offer__data">/{data}</span>
						<div className="offer__button">
							<Form {...props.data} />
						</div>
					</div>
				</div>
			) : (
				<div className={props.data.super === "normal" ? className  :  `${className} ${ className}-${props.data.super}`}>
					{" "}
					<h3 className="offer__online-price">{price}zł</h3>
					<span className="offer__online-data">{data}</span>
					<Form {...props.data} />
				</div>
			)}
		</>
	);
}

export default Box;
