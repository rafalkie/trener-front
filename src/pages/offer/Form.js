import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import {Link} from "react-router-dom";
function Form(props) {
	const { package_name, name, price, data, type, _id } = props;
	const forms = useSelector((state) => state.front.forms);

	const domain = process.env.REACT_APP_DOMAIN;
	const others = useSelector((state) => state.front.others);
	const { z24_id_sprzedawcy, z24_crc } = others;
	const id = forms && forms.find(x => (x.offer_id.find(x => x  === _id)));
	return (
		<div>
			<form method="GET" action="https://sklep.przelewy24.pl/zakup.php">
				<input type="hidden" name="z24_id_sprzedawcy" value={z24_id_sprzedawcy} />
				<input type="hidden" name="z24_crc" value={z24_crc} />
				<input type="hidden" name="z24_return_url" value={`${domain}/forms/${id && id._id}#finish`} />
				<input type="hidden" name="z24_kwota" value={price * 100} />
				<input type="hidden" name="z24_language" value="pl" />
				{type === "online" ? (
					<input type="hidden" name="z24_nazwa" value={`Pakiet online - ${data}`} />
				) : (
					<input type="hidden" name="z24_nazwa" value={`${package_name} - ${data}`} />
				)}
				<input type="submit" value="Wybierz" />
				<br/>	
				<Link style={{"font-size":"14px"}}  to={`/forms/${id && id._id}`}>Formularz po zakupie</Link>
			</form>
		</div>
	);
}

export default Form;
