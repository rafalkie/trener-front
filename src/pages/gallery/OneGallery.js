import React, { useEffect, useState } from "react";
import Carousel, { Modal, ModalGateway } from "react-images";
import { url } from "../../App";


function OneGallery(props) {
  const [data, setData] = useState({
    modalIsOpen: false,
    lightbox: [],
    galleries: props.data.gallery
  });
  const changeData = item => {
    const copyData = data;
    setData({ ...copyData, ...item });
  };
  const toggleModal = () => {
    changeData({ modalIsOpen: !data.modalIsOpen });
  };

  const openModal = async (index) =>{
    const end = props.data.gallery.slice(index);
    const start =  props.data.gallery.slice(0,index);
    const merge =  [...end,...start]
     const lightbox = await merge.map(item => ({
      src: `${url}/uploads/${item.folder}/${item.name}`
    }));
    changeData({ modalIsOpen: true,lightbox })
  }
  const { modalIsOpen, lightbox, galleries } = data;
  const { name } = props.data;
  return (
    <div className="galeries">
      <h3 className="title">{name}</h3>
      <div className="galeries__container">
        {galleries &&
          galleries.map((item, index) => {
            const { folder, name } = item;
            return (
              <div key={index} className="galeries__photo">
                <img
                  key={index}
                  src={`${url}/uploads/${folder}/${name}`}
                  alt=""
                  onClick={()=>openModal(index)}
                />
              </div>
            );
          })}
      </div>
      <ModalGateway>
        {modalIsOpen && (
          <Modal onClose={toggleModal}>
            <Carousel views={lightbox} />
          </Modal>
        )}
      </ModalGateway>
    </div>
  );
}

export default OneGallery;
