import React, { Fragment } from "react";
import Container from "../container";
import {  useSelector } from "react-redux";
import OneGallery from "./OneGallery";

function Gallery() {
	const data = useSelector((state) => state.front.galleries);
	return (
		<div className="container">
			{data.map((item, index) => {
				return (
					<Fragment key={index}>
						<OneGallery  data={item} />
					</Fragment>
				);
			})}
		</div>
	);
}

export default Container({ type: "galeria" })(Gallery);
