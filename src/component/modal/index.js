import React from "react";

function Modal(props) {
  const css = "modal__new";
  const { visible, setVisible, children } = props;
  return (
    <>
      <div
        className={
          visible
            ? `${css}__container`
            : `${css}__container ${css}__container--none`
        }
      >
        <div
          className={`${css}__container ${css}__container-click`}
          onClick={() => setVisible(false)}
        ></div>
        <div className={!visible ? `${css} ${css}-active` : `${css}`}>
          <div className={`${css}__content`}>
            <button
              className={`${css}__close`}
              onClick={() => setVisible(false)}
            >
              <span className="icon-x"></span>
            </button>
            {children}
          </div>
        </div>
      </div>
    </>
  );
}
export default Modal;
