import React from "react";

 const pageTransition = {
  in: {
    opacity: 1,
    y: 0
  },
  out: {
    opacity: 0,
    y: "5px"
  }
};
export default  pageTransition;