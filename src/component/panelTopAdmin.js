import React, { Component } from "react";
import { withRouter } from "react-router-dom";

export const token = () =>  localStorage.getItem('feathers-jwt');
class PanelTopAdmin extends Component {
  redirectPanel = () => {
    window.location.replace("/panel");
  };
  state = {
      code:token()
  }
  logout = () => {
    localStorage.clear();
    this.setState({code:token()})
    window.location.reload(); 
  };
  render() {
    const {code} = this.state;
    return (
      <>
        {code !== null && (
          <div className="barAdmin__top">
            <button
              className="btn btn--logout"
              onClick={this.logout}
            >
              Wyloguj
            </button>
            <button
              className="btn btn--panel"
              onClick={this.redirectPanel}
            >
              Panel
            </button>
          </div>
         )}
      </>
    );
  }
}
export default withRouter(PanelTopAdmin);
