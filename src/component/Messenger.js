import React, { useEffect,useState } from "react";
import PropTypes from "prop-types";


function Messenger(props) {

  const [fbLoaded,setFbLoaded] = useState(false);
  const [shouldShowDialog,setShouldShowDialog] = useState(undefined)

  const removeElementByIds = ids => {
    ids.forEach(id => {
      const element = document.getElementById(id);
      if (element && element.parentNode) {
        element.parentNode.removeChild(element);
      }
    });
  };
  useEffect(() => {
    setFbAsyncInit();
    reloadSDKAsynchronously();
  }, []);

  const setFbAsyncInit = () => {
    const { autoLogAppEvents, xfbml, version } = props;

    window.fbAsyncInit = () => {
      window.FB.init({
        autoLogAppEvents,
        xfbml,
        version: `v${version}`
      });

      setFbLoaded(true);
    };
  };

  const loadSDKAsynchronously = () => {
    const { language } = props;
    /* eslint-disable */
    (function(d, s, id) {
      var js,
        fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = `https://connect.facebook.net/${language}/sdk/xfbml.customerchat.js`;
      fjs.parentNode.insertBefore(js, fjs);
    })(document, "script", "facebook-jssdk");
    /* eslint-enable */
  };

  const removeFacebookSDK = () => {
    removeElementByIds(["facebook-jssdk", "fb-root"]);

    delete window.FB;
  };

  const reloadSDKAsynchronously = () => {
    removeFacebookSDK();
    loadSDKAsynchronously();
  };

  const controlPlugin = () => {
    const { shouldShowDialog } = props;

    if (shouldShowDialog) {
      window.FB.CustomerChat.showDialog();
    } else {
      window.FB.CustomerChat.hideDialog();
    }
  };

  const subscribeEvents = () => {
    const { onCustomerChatDialogShow, onCustomerChatDialogHide } = props;

    if (onCustomerChatDialogShow) {
      window.FB.Event.subscribe(
        "customerchat.dialogShow",
        onCustomerChatDialogShow
      );
    }

    if (onCustomerChatDialogHide) {
      window.FB.Event.subscribe(
        "customerchat.dialogHide",
        onCustomerChatDialogHide
      );
    }
  };

  const createMarkup = () => {
    const {
      pageId,
      htmlRef,
      minimized,
      themeColor,
      loggedInGreeting,
      loggedOutGreeting,
      greetingDialogDisplay,
      greetingDialogDelay
    } = props;

    const refAttribute = htmlRef !== undefined ? `ref="${htmlRef}"` : "";
    const minimizedAttribute =
      minimized !== undefined ? `minimized="${minimized}"` : "";
    const themeColorAttribute =
      themeColor !== undefined ? `theme_color="${themeColor}"` : "";
    const loggedInGreetingAttribute =
      loggedInGreeting !== undefined
        ? `logged_in_greeting="${loggedInGreeting}"`
        : "";
    const loggedOutGreetingAttribute =
      loggedOutGreeting !== undefined
        ? `logged_out_greeting="${loggedOutGreeting}"`
        : "";
    const greetingDialogDisplayAttribute =
      greetingDialogDisplay !== undefined
        ? `greeting_dialog_display="${greetingDialogDisplay}"`
        : "";
    const greetingDialogDelayAttribute =
      greetingDialogDelay !== undefined
        ? `greeting_dialog_delay="${greetingDialogDelay}"`
        : "";

    return {
      __html: `<div
        class="fb-customerchat"
        page_id="${pageId}"
        ${refAttribute}
        ${minimizedAttribute}
        ${themeColorAttribute}
        ${loggedInGreetingAttribute}
        ${loggedOutGreetingAttribute}
        ${greetingDialogDisplayAttribute}
        ${greetingDialogDelayAttribute}
      ></div>`
    };
  };


  if (fbLoaded && shouldShowDialog !== props.shouldShowDialog) {
    document.addEventListener(
      "DOMNodeInserted",
      event => {
        const element = event.target;
        if (
          element.className &&
          typeof element.className === "string" &&
          element.className.includes("fb_dialog")
        ) {
          controlPlugin();
        }
      },
      false
    );
    subscribeEvents();
  }
  // Add a random key to rerender. Reference:
  // https://stackoverflow.com/questions/30242530/dangerouslysetinnerhtml-doesnt-update-during-render
  return <div key={Date()} dangerouslySetInnerHTML={createMarkup()} />;
}

export default Messenger;


Messenger.defaultProps = {
    shouldShowDialog: false,
    htmlRef: undefined,
    minimized: undefined,
    themeColor: undefined,
    loggedInGreeting: undefined,
    loggedOutGreeting: undefined,
    greetingDialogDisplay: undefined,
    greetingDialogDelay: undefined,
    autoLogAppEvents: true,
    xfbml: true,
    version: "2.11",
    language: "pl_PL",
    onCustomerChatDialogShow: undefined,
    onCustomerChatDialogHide: undefined
  };
Messenger.propTypes = {
    pageId: PropTypes.string.isRequired,
    appId: PropTypes.string,

    shouldShowDialog: PropTypes.bool,
    htmlRef: PropTypes.string,
    minimized: PropTypes.bool,
    themeColor: PropTypes.string,
    loggedInGreeting: PropTypes.string,
    loggedOutGreeting: PropTypes.string,
    greetingDialogDisplay: PropTypes.oneOf(["show", "hide", "fade"]),
    greetingDialogDelay: PropTypes.number,
    autoLogAppEvents: PropTypes.bool,
    xfbml: PropTypes.bool,
    version: PropTypes.string,
    language: PropTypes.string,
    onCustomerChatDialogShow: PropTypes.func,
    onCustomerChatDialogHide: PropTypes.func
  };