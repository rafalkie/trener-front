import React from 'react';
import { Link } from '@material-ui/core';


const Social = (props) => {

    const { data, isLoading } = props.others;
    return (
        <div className="social">
            {!isLoading && data.map((item, index) => {
                if (item.name === "facebook" || item.name === "youtube") {
                    return (
                        <div key={index} className="social__box">
                            <a   className={`icon-${item.name}`} href={`${item.text}`} target="_blank"></a>
                        </div>
                    )
                } else return null;
            }
            )}
        </div>
    )
}

export default Social;

