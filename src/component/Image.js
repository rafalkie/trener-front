import React from "react";
import { url } from "../App";

const Image = props => {
  const { item, alt, width = "", height = "", onLoad } = props;
  return (
    <>
      {onLoad !== undefined ? (
        <img
          onLoad={onLoad}
          onError={onLoad}
          width={width}
          height={height}
          src={`${url}/uploads/${item && item.folder}/${item && item.name}`}
          alt={alt}
        />
      ) : (
        <img
          width={width}
          height={height}
          src={`${url}/uploads/${item && item.folder}/${item && item.name}`}
          alt={alt}
        />
      )}
    </>
  );
};
export default Image;
