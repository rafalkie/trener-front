import React, { useState, useRef, useEffect } from "react";

function Expand(props) {
  const [height, setHeight] = useState(0);

  const ref = useRef(null);

  const { text, btn_off, btn_on } = props;
  const [expand, setExpand] = useState(false);
  useEffect(() => {
    setHeight(ref.current.clientHeight);
  });
  return (
    <div className="expand">
      <div
        ref={ref}
        className={expand ? "expand__text" : "expand__text expand__text--off"}
      >
        {text()}
      </div>
      {height > 244 &&
        (expand ? (
          <button className="expand__button" onClick={() => setExpand(false)}>
            {btn_off}
          </button>
        ) : (
          <button className="expand__button" onClick={() => setExpand(true)}>
            {btn_on}
          </button>
        ))}
    </div>
  );
}

export default Expand;
