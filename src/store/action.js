import { getService } from "./services";

export function getDataRequested() {
  return {
    type: "GET_DATA_REQUESTED"
  };
}
export function getDataDone(data,name) {
  return {
    type: "GET_DATA_DONE",
    payload: data,
    name
  };
}
export function changePage(value) {
  return {
    type: "CHANGE_PAGE",
    payload: value,
  };
}
export function getDataFailed(error) {
  return {
    type: "GET_DATA_FAILED",
    payload: error
  };
}
export function loadImage() {
  return {
    type: "LOAD_IMAGE",
  };
}
function getData(name,param = "") {
  return dispatch => {
    dispatch(getDataRequested());
    getService(name+param)
      .then(response => response.data)
      .then(data => {
        dispatch(getDataDone(data,name));
      })
      .catch(error => {
        dispatch(getDataFailed(error));
      });
  };
}

export { getData };
