import types from "./types";

const INITIAL_STATE = {
	others: [],
	faq: [],
	isLoading: false,
	loading: 0,
	loadingImage: 0,
	pagination: {
		active: 1,
		perPage: 6,
		total: 0
	}
};

const taskReducer = (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case types.GET_DATA_REQUESTED:
			return { ...state, isLoading: true };
		case types.GET_DATA_DONE:
			const { payload, name } = action;
			const { active } = state.pagination;
			if (name === "news") {
				return {
					...state,
					isLoading: false,
					loading: state.loading + 1,
					pagination: {
						...state.pagination,
						total: payload.total
					},
					[name]: { ...state.news, [active]: payload.data }
				};
			} else if (name === "others") {
				return {
					...state,
					isLoading: false,
					loading: state.loading + 1,
					[name]: payload[0]
				};
			} else {
				return {
					...state,
					isLoading: false,
					loading: state.loading + 1,
					[name]: payload.data
				};
			}

		case types.GET_DATA_FAILED:
			return { ...state, isLoading: false, loading: state.loading + 1, isError: true };
		case types.LOAD_IMAGE:
			return { ...state, loadingImage: state.loadingImage + 1 };
		case types.CHANGE_PAGE:
			return {
				...state,
				pagination: { ...state.pagination, active: action.payload }
			};
		default:
			return state;
	}
};

export default taskReducer;
