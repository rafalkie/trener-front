import {combineReducers} from 'redux';
import taskReducer from './reducer'
const rootReducer = combineReducers({
    front: taskReducer,
})

export default rootReducer;