
import { createStore, compose, applyMiddleware } from 'redux';
import rootReducer from './reducers';
import thunk from 'redux-thunk';
// const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ &&

// window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({ 
// trace: true, 
// traceLimit: 25 
// }) || compose; 
const composeEnhancer = compose;
export const store = createStore(
  rootReducer,
  composeEnhancer(applyMiddleware(thunk)),
);
