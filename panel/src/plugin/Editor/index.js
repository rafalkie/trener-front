import React, { useState, useEffect } from "react";
import RichTextInput from "ra-input-rich-text";
import toolbarOptions from "./../../components/layout/tolbarOptions";
import Modal from "react-responsive-modal";
import UploadFile from "./../File/component/uploadFile";
import File from "./../File/component/file";
import Alert from "./../../components/alert";
import actions from "./../File/action";
import { connect } from "react-redux";

function Editor(props) {
  const [visible, setVisible] = useState(false);
  const [quill, setQuill] = useState();
  const addQuill = quill => setQuill(quill);
  useEffect(() => {
    const { edytor } = props.list.tabSelect;
    
    if (edytor !== undefined && Object.keys(edytor).length > 0) {
      var range = quill.getSelection();
      if (range) {
        if (range.length == 0) {
          range = range.index;
        } else {
          range =  range.length;
        }
      }
      const url = `${process.env.REACT_APP_API}/uploads/${edytor.folder}/${edytor.name}`;
      quill.clipboard.dangerouslyPasteHTML(range, `<img src="${url}" />`);
      props.clearTabSelected();
      setVisible(false);
    }
  }, [props]);

  useEffect(() => {
    if (quill !== undefined) {
      quill.getModule("toolbar").addHandler("image", function(value) {
        setVisible(true);
      });
    }
  }, [props, quill]);

  const { source,label } = props;
  return (
    <>
      <RichTextInput
        toolbar={toolbarOptions}
        source={source}
        label={label}
        validation={{ required: true }}
        configureQuill={addQuill}
      />

      <Modal
        open={visible}
        onClose={() => setVisible(false)}
        center
        classNames={{ modal: "modalFile" }}
      >
        <div>
          <UploadFile name="edytor" />
          <File name="edytor" task="edit" visible={visible} type="radio" />
          <Alert />
        </div>
      </Modal>
    </>
  );
}

const mapStateToProps = state => ({
  list: state.files
});
const mapDispatchToProps = dispatch => {
  return {
    clearTabSelected: () => dispatch(actions.clearTabSelected())
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Editor);
