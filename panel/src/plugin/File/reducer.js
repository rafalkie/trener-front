import types from "./types";
import { isEmpty } from "./../../components/helper/isEmpty";

const INITIAL_STATE = {
	data: [],
	typeInput: "",
	activePage: 1,
	totalItemsCount: null,
	itemsCountPerPage: 10,
	tabSelect: {},
	open: false,
	alert: [false, "", true], // display,message,{succes or error}
	folders: [],
	activeFolder: "",
	save: false
};

const taskReducer = (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case types.GET_DATA_REQUESTED:
			return { ...state, isLoading: true };
		case types.GET_DATA_DONE_FOLDER:
			return {
				...state,
				isLoading: false,
				folders: action.payload,
				activeFolder: action.payload[0]
			};
		case types.GET_DATA_DONE:
			const { itemsCountPerPage, activePage } = action;
			return {
				...state,
				isLoading: false,
				data: action.payload,
				itemsCountPerPage,
				activePage,
				totalItemsCount: action.total
			};
		case types.GET_DATA_FAILED:
			return { ...state, isLoading: false, isError: true };
		case types.CHANGE_MODAL:
			return {
				...state,
				open: action.open
			};
		case types.CHANGE_FOLDERS_ACTIVE:
			return {
				...state,
				activeFolder: action.activeFolder
			};
		case types.CHANGE_PAGE:
			return {
				...state,
				activePage: action.activePage
			};
		case types.CHANGE_TYPE:
			return {
				...state,
				typeInput: action.typeInput
			};
		case types.SAVE:
			return {
				...state,
				save: action.payload
			};
		case types.CHANGE_COUNT_PER_PAGE:
			return {
				...state,
				itemsCountPerPage: action.payload,activePage: 1
			};
		case types.CLEAR_TAB_SELECTED:
			return {
				...state,
				tabSelect: []
			};
		case types.GET_ALERT:
			const { display, message, status } = action;
			return {
				...state,
				alert: [display, message, status]
			};
		case types.ADD_SELECT_ITEM:
			const { tabSelect, name } = action;
			// if (!isEmpty(tabSelect)) {
			return {
				...state,
				tabSelect: {
					...state.tabSelect,
					[name]: tabSelect
				}
			};
		// } else return {...state,
		// 	tabSelect: {
		// 		...state.tabSelect,
		// 		[name]:{}
		// 	}}

		default:
			return state;
	}
};

export default taskReducer;
