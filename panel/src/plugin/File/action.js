import types from "./types";
import { getFileService } from "./../../service/files";
import { getItemToGalleryService } from "./../../service/gallery";
import { getFoldersService } from "./../../service/folders";
import { isEmpty } from "./../../components/helper/isEmpty";

export function getDataRequested() {
	return {
		type: "GET_DATA_REQUESTED"
	};
}
export function getDataDone(data, itemsCountPerPage, activePage) {
	return {
		type: "GET_DATA_DONE",
		payload: data.data,
		itemsCountPerPage,
		activePage,
		total: data.total
	};
}
export function getDataDoneFolder(data) {
	return {
		type: "GET_DATA_DONE_FOLDER",
		payload: data
	};
}
export function getDataDoneGallery(data) {
	return {
		type: "GET_DATA_DONE_GALLERY",
		payload: data
	};
}

export function getDataFailed(error) {
	return {
		type: "GET_DATA_FAILED",
		payload: error
	};
}
 export function getSave(save) {
	return {
		type: "SAVE",
		payload:save
	};
}
export function changeCountPerPage(count) {
	return {
		type: "CHANGE_COUNT_PER_PAGE",
		payload:count
	};
}

function getFile(itemsCountPerPage, activePage, activeFolder) {
	return (dispatch) => {
		// set state to "loading"
		dispatch(getDataRequested());
		getFileService(itemsCountPerPage, activePage, activeFolder)
			.then((response) => response.data)
			.then((data) => {
				dispatch(getDataDone(data, itemsCountPerPage, activePage));
			})
			.catch((error) => {
				dispatch(getDataFailed(error));
			});
	};
}
function getFolders() {
	return (dispatch) => {
		// set state to "loading"
		dispatch(getDataRequested());
		getFoldersService()
			.then((response) => response.data)
			.then((data) => {
				dispatch(getDataDoneFolder(data));
			})
			.catch((error) => {
				dispatch(getDataFailed(error));
			});
	};
}
function getItemToGallery(name, urlRef, howMuch = undefined) {
	return (dispatch) => {
		dispatch(getDataRequested());
		getItemToGalleryService(name, urlRef)
			.then((response) => response.data)
			.then((data) => {
				dispatch(addSelectItem(undefined, undefined, howMuch, data[name], name));
			})
			.catch((error) => {
				dispatch(getDataFailed(error));
			});
	};
}

const addSelectItem = (e, item, howMuch, tabSelect, name) => {
	let prevTabSelect = [];

	if (e !== undefined) {
		if (howMuch !== "radio") {
			prevTabSelect = tabSelect[name];
			prevTabSelect = !isEmpty(prevTabSelect)
				? prevTabSelect.filter((elem) => elem._id != item._id)
				: [];
			// Dla Galleri
			if (e.target.checked) {
				return {
					type: types.ADD_SELECT_ITEM,
					tabSelect: [...prevTabSelect, item],
					name
				};
			} else {
				return {
					type: types.ADD_SELECT_ITEM,
					tabSelect: [...prevTabSelect],
					name
				};
			}
		}
		// Dla pojedynczych zdjęć
		else {
			if (e.target.checked) {
				return {
					type: types.ADD_SELECT_ITEM,
					tabSelect: item,
					name
				};
			} else {
				return {
					type: types.ADD_SELECT_ITEM,
					tabSelect: {},
					name
				};
			}
		}
	} else {
		return {
			type: types.ADD_SELECT_ITEM,
			tabSelect: tabSelect !== undefined ? tabSelect : {},
			name
		};
	}
};
const changePage = (pageNumber) => {
	return {
		type: types.CHANGE_PAGE,
		activePage: pageNumber
	};
};
const changeType = (typeInput) => {
	return {
		type: types.CHANGE_TYPE,
		typeInput: typeInput
	};
};
const clearTabSelected = () => {
	return {
		type: types.CLEAR_TAB_SELECTED
	};
};
const changeModal = (open) => {
	return {
		type: types.CHANGE_MODAL,
		open
	};
};
const changeActiveFolder = (activeFolder) => {
	return {
		type: types.CHANGE_FOLDERS_ACTIVE,
		activeFolder
	};
};

const getAlert = (display, message, status) => {
	return {
		type: types.GET_ALERT,
		display,
		message,
		status
	};
};
export default {
	changeCountPerPage,
	getFile,
	changeModal,
	getAlert,
	addSelectItem,
	getItemToGallery,
	changePage,
	changeType,
	clearTabSelected,
	getFolders,
	changeActiveFolder,
	getSave
};
