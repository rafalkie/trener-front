import React, { useEffect, useState } from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import { connect } from "react-redux";
import actions from "./../action";
import PhotoLibrary from "@material-ui/icons/PhotoLibrary";
import { isEmpty } from "./../../../components/helper/isEmpty";

function FileItem(props) {
  const {
    getItemToGallery,
    name,
    addSelectItem,
    task,
    changePhoto,
    type,
    title,
    setVisible,
    getSave
  } = props;
  const { tabSelect, open, save,data } = props.list;
  const urlRef = `/${props.resource}/${props.match.params.id}`;
  const delFileWithGallery = id => {
    let gallery = tabSelect[name];
    if (type === "checkbox") {
      gallery = gallery.filter(elem => elem._id !== id);
      addSelectItem(undefined, undefined, undefined, gallery, name);
    }
    if (type === "radio") {
      addSelectItem(undefined, undefined, undefined, {}, name);
    }
  };

  useEffect(() => {
    if (tabSelect[name] !== undefined) {
      if (save) getSave(false);
      if (type === "radio") {
        changePhoto(
          tabSelect[name]._id !== undefined ? tabSelect[name]._id : null
        );
      } else {
        if (tabSelect[name].length > 0) {
          let idTab = tabSelect[name].map(item => item._id);
          changePhoto(idTab);
        } else {
          changePhoto(null);
        }
      }
    } else {
      changePhoto(null);
    }
  }, [props.list.tabSelect]);

  useEffect(() => {
    if (task !== "edit") {
      props.clearTabSelected();
    }
    if (task === "edit") {
      if (!save) getItemToGallery(name, urlRef, task);
    }
    addSelectItem();
  }, []);

  let actualItem = tabSelect[name] !== undefined ? tabSelect[name] : [];
  const ButtonCheckedPhoto = () => (
    <button type="button" className="add" onClick={setVisible}>
      <PhotoLibrary /> <p>Wybierz Zdjęcie</p>
    </button>
  );

  const GalleryFileItem = props => {
    const { item } = props;
    return (
      <>
        {!isEmpty(item) && (
          <div className="files__item files__item--gallery">
            <button
              type="button"
              className="del"
              onClick={() => delFileWithGallery(item._id)}
            >
              <DeleteIcon />
            </button>
            <div className="card-content">
              <img
                src={`${process.env.REACT_APP_API}/uploads/${item.folder}/${item.name}`}
                alt="avatar"
              />
            </div>
          </div>
        )}
      </>
    );
  };

  return (
    <div className="viewPhoto">
      <p className="MuiFormLabel-root" style={{ textAlign: "left",margin:"5px",marginTop:"25px" }}>{title}</p>
      {type === "checkbox" ? (
        <div className="gallery__flex">
          <div className="files__item files__item--gallery">
            <ButtonCheckedPhoto />
          </div>
          {actualItem.length > 0 &&
            actualItem.map((item, index) => (
              <div key={index}>
                <GalleryFileItem item={item} />
              </div>
            ))}
        </div>
      ) : (
        <div className="gallery__flex">
          <div className="files__item files__item--gallery">
            <ButtonCheckedPhoto />
          </div>
          {!isEmpty(actualItem) ? (
            <GalleryFileItem item={actualItem} />
          ) : (
            <GalleryFileItem item={actualItem} />
          )}
        </div>
      )}
    </div>
  );
}

const mapStateToProps = state => ({
  list: state.files
});

const mapDispatchToProps = dispatch => {
  return {
    changeType: typeInput => dispatch(actions.changePage(typeInput)),
    getSave: save => dispatch(actions.getSave(save)),
    getItemToGallery: (name, urlRef, task) =>
      dispatch(actions.getItemToGallery(name, urlRef, task)),
    addSelectItem: (e, item, howMuch, tabSelect, name) =>
      dispatch(actions.addSelectItem(e, item, howMuch, tabSelect, name)),
    clearTabSelected: () => dispatch(actions.clearTabSelected())
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(FileItem);