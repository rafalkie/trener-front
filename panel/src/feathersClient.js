import feathers from 'feathers-client';

const host = `${process.env.REACT_APP_API}`;
export const domain = `${process.env.REACT_APP_DOMAIN}`;

export default feathers()
    .configure(feathers.hooks())
    .configure(feathers.rest(host).fetch(window.fetch.bind(window)))
    .configure(feathers.authentication({ jwtStrategy: 'jwt', storage: window.localStorage }));