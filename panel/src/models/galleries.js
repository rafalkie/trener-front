import React, { useState, Fragment } from "react";
import {
  List,
  FormDataConsumer,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  TextInput,
  Edit,
  DeleteButton
} from "react-admin";
import ModalFile from "./../components/ModalFile";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import { imgUrl, cardStyle, isEmptyTab, exist } from "./../components/helper";
import PhotoMax from "./../components/layout/PhotoMax";
import CustomToolbar from "./../components/layout/CustomToolbar";
import DragAndDrop from "./../components/dragAndDrop/index";
import { dataProvider } from "./../App";
import PostActions from "../components/layout/PostAction";
import ListDropDown from "../components/dragAndDrop/ListDropdown";
import { useDispatch } from "react-redux";
import { useForm } from "react-final-form";

const GalleryGrid = ({ ids, data, basePath }) => (
  <div style={{ margin: "1em" }}>
    {ids.map(id => (
      <Card key={id} style={cardStyle}>
        <CardHeader title={<TextField record={data[id]} source="name" />} />
        <CardContent className="card-content">
          {exist(data[id].gallery) && !isEmptyTab(data[id].gallery) && (
            <img
              src={`${imgUrl}${data[id].gallery[0].folder}/${data[id].gallery[0].name}`}
            />
          )}
        </CardContent>
        <CardActions
          style={{ display: "flex", justifyContent: "space-around" }}
        >
          <DeleteButton
            basePath={basePath}
            record={data[id]}
            resource="galleries"
          />
          <EditButton
            resource="galleries"
            basePath={basePath}
            record={data[id]}
          />
        </CardActions>
      </Card>
    ))}
  </div>
);

export function GalleryList(props) {
  const [visible, setVisible] = useState(true);
  return (
    <>
      {visible ? (
        <div>
          <List
            {...props}
            sort={{ field: "position", order: "ASC" }}
            actions={
              <PostActions
                visible={visible}
                order={() => setVisible(!visible)}
                {...props}
              />
            }
          >
            <GalleryGrid />
          </List>
        </div>
      ) : (
        <List
          {...props}
          sort={{ field: "position", order: "ASC" }}
          actions={
            <PostActions
              visible={visible}
              order={() => setVisible(!visible)}
              {...props}
            />
          }
        >
          <ListDropDown name={"name"} setVisible={() => setVisible(true)} />
        </List>
      )}
    </>
  );
}

const Origin = ({ formData, ...rest }) => {
  const form = useForm();
  const { task } = rest;
  return (
    <Fragment>
      <TextInput fullWidth source="name" label="Nazwa" />

      <ModalFile
        changePhoto={value => form.change("gallery_id", value)}
        {...rest}
        file={{
          title: "Zdjęcia w galleri",
          name: "gallery",
          type: "checkbox",
          task
        }}
      />

      <PhotoMax height="800px" width="1200px" />
    </Fragment>
  );
};
export function GalleryEdit(props) {
  return (
    <Edit title={<GalleryTitle />} {...props}>
      <SimpleForm toolbar={<CustomToolbar />}>
        <FormDataConsumer>
          {formDataProps => (
            <Origin {...formDataProps} {...props} task="edit" />
          )}
        </FormDataConsumer>
      </SimpleForm>
    </Edit>
  );
}

const GalleryTitle = ({ record }) => {
  return <span>Edycja referencji {record ? `"${record.name}"` : ""}</span>;
};

export function GalleryCreate(props) {
  return (
    <Create title="Dodaj referencje" {...props}>
      <SimpleForm>
        <FormDataConsumer>
          {formDataProps => (
            <Origin {...formDataProps} {...props} task="create" />
          )}
        </FormDataConsumer>
      </SimpleForm>
    </Create>
  );
}
