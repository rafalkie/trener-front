import React, { useState } from "react";
import {
  List,
  Datagrid,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  TextInput,
  FormDataConsumer
} from "react-admin";
import BookIcon from "@material-ui/icons/Book";
import ModalFile from "./../components/ModalFile";
import PhotoMax from "./../components/layout/PhotoMax";
import CustomToolbar from "./../components/layout/CustomToolbar";
import ListDropDown from "../components/dragAndDrop/ListDropdown";
import PostActions from "../components/layout/PostAction";
import { Fragment } from "react";
import { useForm } from "react-final-form";

export const PostIcon = BookIcon;

export function ReferenceList(props) {
  const [visible, setVisible] = useState(true);
  return (
    <>
      {visible ? (
        <div>
          <List
            {...props}
            sort={{ field: "position", order: "ASC" }}
            title="Referencje"
            actions={
              <PostActions
                visible={visible}
                order={() => setVisible(!visible)}
                {...props}
              />
            }
          >
            <Datagrid>
              <TextField source="name" label="Imię i Nazwisko" />
              <TextField source="description" label="Opis" />
              <EditButton basePath="/references" />
            </Datagrid>
          </List>
        </div>
      ) : (
        <List
          {...props}
          sort={{ field: "position", order: "ASC" }}
          title="Referencje"
          actions={
            <PostActions
              visible={visible}
              order={() => setVisible(!visible)}
              {...props}
            />
          }
        >
          <ListDropDown name={"name"} setVisible={() => setVisible(true)} />
        </List>
      )}
    </>
  );
}
const Origin = ({ formData, ...rest }) => {
  const form = useForm();
  const { task } = rest;
  return (
    <Fragment>
      <TextInput source="name" label="Imię i Nazwisko" />
      <TextInput multiline fullWidth source="description" label="Opis" />
      <ModalFile
        changePhoto={value => form.change("image_id", value)}
        {...rest}
        file={{
          title: "Aktualny avatar",
          name: "image",
          type: "radio",
          task
        }}
      />

      <PhotoMax height="64px" width="64px" />
    </Fragment>
  );
};
export function ReferenceEdit(props) {
  return (
    <Edit title={<ReferenceTitle />} {...props}>
      <SimpleForm toolbar={<CustomToolbar />}>
        <FormDataConsumer>
          {formDataProps => (
            <Origin {...formDataProps} {...props} task="edit" />
          )}
        </FormDataConsumer>
      </SimpleForm>
    </Edit>
  );
}

const ReferenceTitle = ({ record }) => {
  return <span>Edycja referencji {record ? `"${record.name}"` : ""}</span>;
};

export const ReferenceCreate = props => (
  <Create title="Dodaj referencje" {...props}>
    <SimpleForm>
      <FormDataConsumer>
        {formDataProps => (
          <Origin {...formDataProps} {...props} task="create" />
        )}
      </FormDataConsumer>
    </SimpleForm>
  </Create>
);
