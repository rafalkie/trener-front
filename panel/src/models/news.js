import React, { useState } from "react";
import {
  List,
  Edit,
  Create,
  SimpleForm,
  TextField,
  Datagrid,
  TextInput,
  FormDataConsumer,
  EditButton,
  DateField
} from "react-admin";
import ModalFile from "../components/ModalFile";
import { Fragment } from "react";
import CustomToolbar from "../components/layout/CustomToolbar";
import { useForm } from "react-final-form";
import Editor from "./../plugin/Editor";

export function NewsList(props) {
  return (
    <List {...props}  title="Aktualności" exporter={false} sort={{ field: 'createdAt', order: 'DESC' }} >
      <Datagrid>
        <TextField source="title" label="Tytuł" />
        <DateField source="createdAt" label="Data utworzenia" />
        <DateField source="updatedAt" label="Data ostatniej aktualizacji" />
        <EditButton basePath="/news" />
      </Datagrid>
    </List>
  );
}

const Origin = ({ formData, ...rest }) => {
  const form = useForm();
  const { task } = rest;
  return (
    <Fragment>
      <TextInput fullWidth source="title" label="Tytuł" />
      <Editor source="text" />
      <ModalFile
        changePhoto={value => form.change("image_id", value)}
        {...rest}
        file={{
          title: "Zdjęcie",
          name: "image",
          type: "radio",
          task
        }}
      />
      {/* <PhotoMax height="150px" width="220px" /> */}
    </Fragment>
  );
};
export function NewsEdit(props) {
  return (
    <Edit title={<NewsTitle />} {...props}>
      <SimpleForm toolbar={<CustomToolbar />}>
        <FormDataConsumer>
          {formDataProps => (
            <Origin {...formDataProps} {...props} task="edit" />
          )}
        </FormDataConsumer>
      </SimpleForm>
    </Edit>
  );
}

const NewsTitle = ({ record }) => {
  return <span>Edycja wpisu {record ? `"${record.title}"` : ""}</span>;
};

function NewsCreate(props) {
  return (
    <Create title="Dodaj wpis" {...props}>
      <SimpleForm redirect="/news">
        <FormDataConsumer>
          {formDataProps => (
            <Origin {...formDataProps} {...props} task="create" />
          )}
        </FormDataConsumer>
      </SimpleForm>
    </Create>
  );
}

export default NewsCreate;
