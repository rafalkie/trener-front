import React from "react";
import {
  Edit,
  TextInput,
  FormDataConsumer,
  TabbedForm,
  FormTab,
  TabbedFormTabs
} from "react-admin";
import ModalFile from "../components/ModalFile";
import PhotoMax from "../components/layout/PhotoMax";
import CustomToolbar from "./../components/layout/CustomToolbar";
import { useForm } from "react-final-form";
import Editor from "../plugin/Editor";

const OtherTitle = ({ record }) => {
  return <span>Edycja ustawień</span>;
};
const OrderAboutMe = ({ formData, ...rest }) => {
  const form = useForm();

  return (
    <>
      <TextInput multiline fullWidth source="about_me_title" label="Tytuł" />
      <TextInput
        multiline
        fullWidth
        source="about_me_subtitle"
        label="Podtytuł"
      />
      <TextInput
        multiline
        fullWidth
        source="about_me_description"
        label="Opis"
      />

      <ModalFile
        changePhoto={value => form.change("image_second_id", value)}
        {...rest}
        file={{
          title: "Pierwsze zdjęcie",
          name: "image_second",
          type: "radio",
          task: "edit"
        }}
      />
    </>
  );
};
const OrderFooter = ({ formData, ...rest }) => {
  const form = useForm();

  return (
    <>
      <TextInput
        multiline
        fullWidth
        source="footer_description"
        label="Tekst"
      />
      <TextInput source="footer_email" label="Email" />
      <TextInput source="footer_tel" label="Telefon" />
      <TextInput fullWidth source="footer_fb" label="Link Facebook" />
      <TextInput fullWidth source="footer_instagram" label="Link Instagram" />
      <ModalFile
        changePhoto={value => form.change("image_performance_id", value)}
        {...rest}
        file={{
          title: "Pierwsze zdjęcie",
          name: "image_performance",
          type: "radio",
          task: "edit"
        }}
      />
    </>
  );
};
const OrderOrigin = ({ formData, ...rest }) => {
  const form = useForm();

  return (
    <>
      <TextInput fullWidth source="first_section_title" label="Tytuł" />
      <TextInput fullWidth source="first_section_subtitle" label="Podtytuł" />
      <>
        {}
        <ModalFile
          changePhoto={value => form.change("image_first_id", value)}
          {...rest}
          file={{
            title: "Pierwsze zdjęcie",
            name: "image_first",
            type: "radio",
            task: "edit"
          }}
        />
        {/* <PhotoMax height="64px" width="64px" /> */}
        <ModalFile
          changePhoto={value => form.change("logo_id", value)}
          {...rest}
          file={{
            title: "Pierwsze zdjęcie",
            name: "logo",
            type: "radio",
            task: "edit"
          }}
        />
        {/* <PhotoMax height="64px" width="64px" /> */}
      </>
    </>
  );
};
const OrderPlugin = ({ formData, ...rest }) => {
  return (
    <>
      <TextInput
        fullWidth
        source="z24_id_sprzedawcy"
        label="Przelewy 24 - Id sprzedawcy"
      />
      <TextInput
        fullWidth
        source="z24_crc"
        label="Przelewy 24 - Klucz do zamówień"
      />
      <TextInput
        fullWidth
        source="messenger_id"
        label="Messenger - Id strony"
      />
      <TextInput
        fullWidth
        source="captcha_gmail"
        label="Gmail - Kod reCAPTCHA v2"
      />
    </>
  );
};
const OrderPerformance = ({ formData, ...rest }) => {
  const form = useForm();

  return (
    <ModalFile
      changePhoto={value => form.change("image_performance_id", value)}
      {...rest}
      file={{
        title: "Pierwsze zdjęcie",
        name: "image_performance",
        type: "radio",
        task: "edit"
      }}
    />
  );
};

const OrderRegulations = ({ formData, ...rest }) => {
  return <Editor source="regulations" label="Regulamin" />;
};
const OrderForms = ({ formData, ...rest }) => {
  return (
    <>
      <Editor source="forms_thx" label="Podziekowanie po zakupie" />
      <Editor source="forms_title" label="Tytuł formularza" />
    </>
  );
};
export function OtherEdit(props) {
  return (
    <Edit title={<OtherTitle />} {...props}>
      <TabbedForm
      tabs={<TabbedFormTabs scrollable={true} />}
        redirect="/others/5deb86a5df9ab434407539f5"
        toolbar={<CustomToolbar />}

      >
        <FormTab label="Pierwsza sekcja">
          <FormDataConsumer>
            {formDataProps => <OrderOrigin {...formDataProps} {...props} />}
          </FormDataConsumer>
        </FormTab>
        <FormTab label="O mnie">
          <FormDataConsumer>
            {formDataProps => <OrderAboutMe {...formDataProps} {...props} />}
          </FormDataConsumer>
        </FormTab>
        <FormTab label="Osiągniecia">
          <FormDataConsumer>
            {formDataProps => (
              <OrderPerformance {...formDataProps} {...props} />
            )}
          </FormDataConsumer>
        </FormTab>
        <FormTab label="Stopka">
          <FormDataConsumer>
            {formDataProps => <OrderFooter {...formDataProps} {...props} />}
          </FormDataConsumer>
        </FormTab>
        <FormTab label="Formularz">
          <FormDataConsumer>
            {formDataProps => <OrderForms {...formDataProps} {...props} />}
          </FormDataConsumer>
        </FormTab>

        <FormTab label="Regulamin">
          <FormDataConsumer>
            {formDataProps => (
              <OrderRegulations {...formDataProps} {...props} />
            )}
          </FormDataConsumer>
        </FormTab>

        <FormTab label="Dodatki">
          <FormDataConsumer>
            {formDataProps => <OrderPlugin {...formDataProps} {...props} />}
          </FormDataConsumer>
        </FormTab>
      </TabbedForm>
    </Edit>
  );
}
