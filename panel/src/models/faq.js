import React, { useState } from 'react';
import { List, Datagrid, Edit, Create, SimpleForm, TextField, EditButton,TextInput } from 'react-admin';
import BookIcon from '@material-ui/icons/Book';
import PostActions from '../components/layout/PostAction';
import ListDropDown from '../components/dragAndDrop/ListDropdown';
export const PostIcon = BookIcon;

export function FaqList(props) {
    const [visible, setVisible] = useState(true);
    return (
      <>
        {visible ? (
          <div>
            <List {...props} sort={{ field: "position", order: "ASC" }} title="FAQ"  actions={<PostActions visible={visible} order={() => setVisible(!visible)} {...props} />}>
                <Datagrid >
                    <TextField source="question" label="Pytanie" />
                    <TextField source="answer" label="Odpowiedź" />
                    <EditButton basePath="/faq" />
                </Datagrid>
            </List>
          </div>
        ) : (
          <List {...props} sort={{ field: "position", order: "ASC" }} title="FAQ" actions={<PostActions visible={visible} order={() => setVisible(!visible)} {...props} />}>
            <ListDropDown name={"question"} setVisible={()=>setVisible(true)} />
          </List>
        )}
      </>
    );
  }
;

const FaqTitle = ({ record }) => {
    return <span>Edycja FAQ {record ? `"${record.name}"` : ''}</span>;
};

export const FaqEdit = (props) => (
    <Edit title={<FaqTitle />} {...props}>
        <SimpleForm>
            <TextInput multiline fullWidth source="question" label="Pytanie" />
            <TextInput multiline fullWidth source="answer" label="Odpowiedź" />
        </SimpleForm>
    </Edit>
);

export const FaqCreate = (props) => (
    <Create title="Dodaj FAQ" {...props}>
        <SimpleForm redirect="/faq">
            <TextInput multiline fullWidth source="question" label="Pytanie" />
            <TextInput multiline fullWidth source="answer" label="Odpowiedź" />
        </SimpleForm>
    </Create>
);