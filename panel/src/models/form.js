import React from "react";
import {
  List,
  Datagrid,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  TextInput,
  ReferenceInput,
  SelectInput
} from "react-admin";
import BookIcon from "@material-ui/icons/Book";
import { ArrayInput, SimpleFormIterator, AutocompleteInput } from "react-admin";

export const PostIcon = BookIcon;

export function FormList(props) {
  return (
    <List {...props} title="Ankiety" exporter={false}>
      <Datagrid>
        <TextField source="question" label="Pytania" />
        <EditButton basePath="/form" />
      </Datagrid>
    </List>
  );
}
const FormTitle = ({ record }) => {
  return <span>Edycja Ankiet {record ? `"${record.name}"` : ""}</span>;
};

export const FormEdit = props => (
  <Edit title={<FormTitle />} {...props}>
    <SimpleForm>
    {/* <ArrayInput source="questions" optionText="Pytania">
        <SimpleFormIterator>
        <TextInput multiline fullWidth source="question" />
        </SimpleFormIterator>
      </ArrayInput> */}
      <ReferenceInput label="Oferta" source="offer_id" reference="offer" allowEmpty>
    <AutocompleteInput inputValueMatcher={() => null} optionText="type" options={{ suggestionsContainerProps: { disablePortal: true, modifiers: { keepTogether: { enabled: true } } } }} />
</ReferenceInput>
    </SimpleForm>
  </Edit>
);

export const FormCreate = props => (
  <Create title="Dodaj Ankiete" {...props}>
    <SimpleForm redirect="/form">
      <ArrayInput source="questions"  >
        <SimpleFormIterator >
          <TextInput multiline fullWidth source="question" label="Pytanie" />
        </SimpleFormIterator>
      </ArrayInput>
      <ReferenceInput label="Oferta" source="offer_id" reference="offer">
    <SelectInput optionText="name" />
</ReferenceInput>
    </SimpleForm>
  </Create>
);
