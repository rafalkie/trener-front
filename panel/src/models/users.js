import React from 'react';
import { List, Datagrid, Edit, Create, SimpleForm, TextField, EditButton } from 'react-admin';
import BookIcon from '@material-ui/icons/Person';
import RichTextInput from 'ra-input-rich-text';
import { Toolbar,  TextInput,DisabledInput } from 'react-admin';

export const PostIcon = BookIcon;


export const UsersList = (props) => (
    <List {...props} title="Użytkownicy" exporter={false}  >
        <Datagrid  >
            <TextField source="email" label="Email" />
            <TextField source="userroles" label="Rola" />
            <TextField source="isVerified" label="Weryfikacja" />
            <EditButton basePath="/users" />
        </Datagrid>
    </List>
);

const UsersTitle = ({ record }) => {
    return <span>Edycja tekstu {record ? `"${record.name}"` : ''}</span>;
};

export const UsersEdit = (props) => (
    <Edit title={<UsersTitle />} {...props}  >
         <SimpleForm >
            <TextInput source="email" label="Email" />
            <TextInput source="userroles" label="Rola" />
            <TextInput source="isVerified" label="Weryfikacja" />
        </SimpleForm>
    </Edit>
);

export const UsersCreate = (props) => (
    <Create title="Dodaj tekst" {...props} >
        <SimpleForm>
            <TextInput source="email" label="Email" />
            <TextInput source="userroles" label="Rola" />
            <TextInput source="password" label="Hasło" type="password" />
        </SimpleForm>
    </Create>
);