import React, { useState } from "react";
import {
  List,
  Datagrid,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  TextInput
} from "react-admin";
import PostActions from "../components/layout/PostAction";
import ListDropDown from "../components/dragAndDrop/ListDropdown";


export function AchievementList(props) {
  const [visible, setVisible] = useState(true);
  return (
    <>
      {visible ? (
        <div>
          <List {...props} sort={{ field: "position", order: "ASC" }} title="Osiągniecia"  actions={<PostActions visible={visible} order={() => setVisible(!visible)} {...props} />}>
            <Datagrid>
              <TextField source="name" label="Nazwa" />
              <TextField source="number" label="Liczba" />
              <EditButton basePath="/achievements" />
            </Datagrid>
          </List>
        </div>
      ) : (
        <List {...props} sort={{ field: "position", order: "ASC" }} title="Osiągniecia" actions={<PostActions visible={visible} order={() => setVisible(!visible)} {...props} />}>
          <ListDropDown name={"name"} setVisible={()=>setVisible(true)} />
        </List>
      )}
    </>
  );
}
;
export const AchievementEdit = props => (
  <Edit title={<AchievementTitle />} {...props}>
    <SimpleForm>
      <TextInput source="name" label="Nazwa" />
      <TextInput type="number" source="number" label="liczba" />
    </SimpleForm>
  </Edit>
);
const AchievementTitle = ({ record }) => {
  return (
    <span>Edycja w czym poamagasz {record ? `"${record.name}"` : ""}</span>
  );
};

export const AchievementCreate = props => (
  <Create title="Dodaj w czym pomagasz" {...props}>
    <SimpleForm>
      <TextInput source="name" label="Nazwa" />
      <TextInput type="number" source="number" label="liczba" />
    </SimpleForm>
  </Create>
);

