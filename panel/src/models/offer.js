import React, { useState } from "react";
import {
	List,
	Datagrid,
	Edit,
	Create,
	SimpleForm,
	TextField,
	EditButton,
	BooleanInput,
	TextInput,
	SelectInput,
	FormDataConsumer
} from "react-admin";
import BookIcon from "@material-ui/icons/Book";
import PostActions from "../components/layout/PostAction";
import ListDropDown from "../components/dragAndDrop/ListDropdown";
import RichTextInput from "ra-input-rich-text";
import toolbarOptions from "../components/layout/tolbarOptions";
import Editor from "../plugin/Editor";
export const PostIcon = BookIcon;

export function OfferList(props) {
	const [visible, setVisible] = useState(true);
	return (
		<>
			{visible ? (
				<div>
					<List
						{...props}
						sort={{ field: "position", order: "ASC" }}
						title="Oferta"
						actions={
							<PostActions visible={visible} order={() => setVisible(!visible)} {...props} />
						}
					>
						<Datagrid>
							<TextField source="package_name" label="Nazwa pakietu" />
							<TextField source="name" label="Tytuł" />
							<TextField source="price" label="Cena" />
							<TextField source="data" label="Długośc pakietu" />
							<TextField source="type" label="Typ" />
							<TextField source="super" label="Wyróżniony" />
							<EditButton basePath="/offer" />
						</Datagrid>
					</List>
				</div>
			) : (
				<List
					{...props}
					sort={{ field: "position", order: "ASC" }}
					title="Oferta"
					actions={<PostActions visible={visible} order={() => setVisible(!visible)} {...props} />}
				>
					<ListDropDown
						name={"package_name"}
						optionalName={"data"}
						setVisible={() => setVisible(true)}
					/>
				</List>
			)}
		</>
	);
}
const OfferTitle = ({ record }) => {
	return <span>Edycja Oferty {record ? `"${record.name}"` : ""}</span>;
};

export const OfferEdit = (props) => (
	<Edit title={<OfferTitle />} {...props}>
		<SimpleForm>
			<SelectInput
				source="type"
				label="Typ"
				choices={[
					{ id: "normal", name: "Normalny" },
					{ id: "online", name: "Współpraca online" }
				]}
			/>
			<FormDataConsumer>
				{({ formData }) =>
					formData.type === "online" ? (
						<>
							<SelectInput
								source="super"
								label="Kolor"
								defaultValue="normal"
								choices={[
									{ id: "normal", name: "normalny" },

									{ id: "black", name: "czarny" },
									{ id: "gold", name: "złoty" }
								]}
							/>{" "}
							<TextInput
								fullWidth
								source="package_name"
								label="Nazwa pakietu (Potrzebna jedynie do identyfikacji ankiety)"
							/>{" "}
							<br />
							<TextInput source="price" label="Cena" /> <br />
							<TextInput source="data" label="Długośc pakietu" />
							<br />
						</>
					) : (
						<>
							<SelectInput
								source="super"
								label="Kolor"
								defaultValue="normal"
								choices={[
									{ id: "normal", name: "normalny" },

									{ id: "black", name: "czarny" },
									{ id: "gold", name: "złoty" }
								]}
							/>{" "}
							<TextInput source="package_name" label="Nazwa pakietu" /> <br />
							<TextInput source="name" label="Tytuł" /> <br />
							<TextInput source="price" label="Cena" /> <br />
							<TextInput source="data" label="Długośc pakietu" /> <br />
							<Editor source="description" />
						</>
					)
				}
			</FormDataConsumer>
		</SimpleForm>
	</Edit>
);

export const OfferCreate = (props) => (
	<Create title="Dodaj Oferte" {...props}>
		<SimpleForm redirect="/offer">
			<SelectInput
				source="type"
				label="Typ"
				choices={[
					{ id: "normal", name: "Normalny" },
					{ id: "online", name: "Współpraca online" }
				]}
			/>
			<FormDataConsumer>
				{({ formData }) =>
					formData.type === "online" ? (
						<>
							<SelectInput
								source="super"
								label="Kolor"
								defaultValue="normal"
								choices={[
									{ id: "normal", name: "normalny" },

									{ id: "black", name: "czarny" },
									{ id: "gold", name: "złoty" }
								]}
							/>
							<TextInput
								fullWidth
								source="package_name"
								label="Nazwa pakietu (Potrzebna jedynie do identyfikacji ankiety)"
							/>{" "}
							<br />
							<TextInput source="price" label="Cena" /> <br />
							<TextInput source="data" label="Długośc pakietu" />
							<br />
						</>
					) : (
						<>
							<SelectInput
								source="super"
								label="Kolor"
								defaultValue="normal"
								choices={[
									{ id: "normal", name: "normalny" },
									{ id: "black", name: "czarny" },
									{ id: "gold", name: "złoty" }
								]}
							/>
							<TextInput source="package_name" label="Nazwa pakietu" /> <br />
							<TextInput source="name" label="Tytuł" /> <br />
							<TextInput source="price" label="Cena" /> <br />
							<TextInput source="data" label="Długośc pakietu" /> <br />
							<Editor source="description" />
						</>
					)
				}
			</FormDataConsumer>
		</SimpleForm>
	</Create>
);
