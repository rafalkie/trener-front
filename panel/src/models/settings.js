import React, { Component } from 'react';
import ChangePassword from './../components/actionAccount/ChangePassword'
import IdentityChange from '../components/actionAccount/identityChange';
import { Route, Switch, Redirect } from "react-router-dom";
import { NavTab } from "react-router-tabs";
import Alert from './../components/alert'
class Settings extends Component {

  render() {
    const { props } = this;
    const pathname = this.props.match.url;
    return (
      <>
        <div className="tab">
          <NavTab to={`${pathname}`} exact>Hasło</NavTab>
          <NavTab to={`${pathname}/2`}>Email</NavTab>
        </div>
        <div className="layout settingsAccount" >

          <Switch>
            <Route
              exact
              path={`/${pathname}`}
              render={() => <Redirect replace to={`${pathname}`} />}
            />
            <Route  {...props} path={`${pathname}`} render={() => { return <ChangePassword /> }} exact />
            <Route path={`${pathname}/2`} render={() => { return <IdentityChange /> }} />
          </Switch>
        </div>
      </>
    )
  }
}

export default Settings;