import React from "react";
import {
	List,
	Datagrid,
	Edit,
	Create,
	SimpleForm,
	TextField,
	EditButton,
	TextInput
} from "react-admin";
import BookIcon from "@material-ui/icons/Book";
import { ArrayInput, SimpleFormIterator, DateInput } from "react-admin";
import { ReferenceArrayInput, ReferenceArrayField,SelectArrayInput  } from "react-admin";
import { ChipField, SingleFieldList } from 'react-admin';

export const PostIcon = BookIcon;

export function FormsList(props) {
	return (
		<List {...props} title="Ankiety" exporter={false}>
			<Datagrid>
			<ReferenceArrayField label="Pakiety" source="offer_id" reference="offer" >
        <SingleFieldList>
                    <ChipField source="package_name" />
                </SingleFieldList>
			</ReferenceArrayField>
				<EditButton basePath="/forms" />
			</Datagrid>
		</List>
	);
}
const FormsTitle = ({ record }) => {
	return <span>Edycja Ankiety</span>;
};

export const FormsEdit = (props) => (
	<Edit title={<FormsTitle />} {...props}>
		<SimpleForm>
			<ArrayInput source="questions" label="Pytanie w ankiecie" >
				<SimpleFormIterator>
					<TextInput source="question" label="Pytanie"  />
				</SimpleFormIterator>
			</ArrayInput>
			<ReferenceArrayInput source="offer_id" reference="offer" label="Pakiet"  >
				<SelectArrayInput optionText="package_name" label="Pakiet"  />
			</ReferenceArrayInput>
		</SimpleForm>
	</Edit>
);

export const FormsCreate = (props) => (
	<Create title="Dodaj Ankiete" {...props}>
		<SimpleForm redirect="/forms">
		<ArrayInput source="questions">
				<SimpleFormIterator>
					<TextInput source="question" />
				</SimpleFormIterator>
			</ArrayInput>
			<ReferenceArrayInput source="offer_id" reference="offer" >
      <SelectArrayInput optionText="package_name"  label="Pakiet" />
			</ReferenceArrayInput>
		</SimpleForm>
	</Create>
);
