import React, { useState, Fragment } from "react";
import {
  List,
  Datagrid,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  FormDataConsumer,
  SelectInput,
  BooleanInput,
  TextInput
} from "react-admin";
import BookIcon from "@material-ui/icons/Book";
import PostActions from "../components/layout/PostAction";
import ListDropDown from "../components/dragAndDrop/ListDropdown";
import ModalFile from "./../components/ModalFile";
import PhotoMax from "./../components/layout/PhotoMax";
import Editor from "./../plugin/Editor";
import { useForm } from "react-final-form";

export const PostIcon = BookIcon;

export function PageList(props) {
  const [visible, setVisible] = useState(true);
  return (
    <>
      {visible ? (
        <div>
          <List
            {...props}
            sort={{ field: "position", order: "ASC" }}
            title="Strony"
            actions={
              <PostActions
                visible={visible}
                order={() => setVisible(!visible)}
                {...props}
              />
            }
          >
            <Datagrid>
              <TextField source="name" label="Nazwa strony" />

              <EditButton basePath="/page" />
            </Datagrid>
          </List>
        </div>
      ) : (
        <List
          {...props}
          sort={{ field: "position", order: "ASC" }}
          title="Strony"
          actions={
            <PostActions
              visible={visible}
              order={() => setVisible(!visible)}
              {...props}
            />
          }
        >
          <ListDropDown name={"name"} setVisible={() => setVisible(true)} />
        </List>
      )}
    </>
  );
}
const PageTitle = ({ record }) => {
  return <span>Edycja Strony {record ? `"${record.name}"` : ""}</span>;
};
const Origin = ({ formData, ...rest }) => {
  const form = useForm();
  const {task} = rest;
  return (
    <Fragment>
      <TextInput source="name" label="Nazwa" />
      <TextInput fullWidth source="subtitle" label="Podtytuł" />
      <BooleanInput
        label="Widoczność na stronie"
        source="visible"
        defaultValue={true}
      />
      <Editor source="text" />
      <SelectInput
        source="module"
        label="Moduł"
        choices={[
          { id: "oferta", name: "Oferta" },
          { id: "galeria", name: "Galeria" },
          { id: "aktualnosci", name: "Aktualności" },
          { id: "strona", name: "Strona" },
          { id: "o-mnie", name: "O mnie" }
        ]}
        defaultValue="page"
      />
      <ModalFile
        changePhoto={value => form.change("image_id", value)}
        {...rest}
        file={{
          title: "Aktualny avatar",
          name: "image",
          type: "radio",
          task
        }}
      />
      <PhotoMax height="600px" width="1600px" />
    </Fragment>
  );
};
export function PageEdit(props) {
  return (
    <Edit title={<PageTitle />} {...props}>
      <SimpleForm>
        <FormDataConsumer>
          {formDataProps => <Origin {...formDataProps} {...props} task="edit"  />}
        </FormDataConsumer>
      </SimpleForm>
    </Edit>
  );
}

export function PageCreate(props) {
  return (
    <Create title="Dodaj Strone" {...props}>
      <SimpleForm>
        <FormDataConsumer>
          {formDataProps => <Origin {...formDataProps} {...props} task="create"  />}
        </FormDataConsumer>
      </SimpleForm>
    </Create>
  );
}
