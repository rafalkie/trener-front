import axios from "axios";

const token = () =>  localStorage.getItem('feathers-jwt');
const url = process.env.REACT_APP_API;

export const getFileService = (itemsCountPerPage, activePage, activeFolder) => {
  return axios.get(`${url}/uploads/?folder=${activeFolder}&$limit=${itemsCountPerPage}&$skip=${(activePage - 1) * itemsCountPerPage}&$sort[createdAt]=-1`, {
    headers: {
      Authorization: token(),
    },
  })
};

export const delFileService = (id) => {
  return axios.delete(`${url}/uploads/${id}`, {
    headers: {
      Authorization: token(),
    }
  })
}

export const uploadFileService = (data) => {
  return axios.post(`${url}/uploads`, data, {
    headers: {
      Authorization: token(),
    },
  })
}