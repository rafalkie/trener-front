import axios from "axios";

const token = () =>  localStorage.getItem('feathers-jwt');
const url = process.env.REACT_APP_API;

export const getFoldersService =  () => {
  return  axios.get(`${url}/folder`, {
    headers: {
      Authorization: token(),
    }
  }
  )
}

export const deleteFolderService = (folder) => {
  return axios.delete(`${url}/folder/${folder}`, {
    headers: {
      Authorization: token(),
    },
  })
}

export const newFolderService = (nameNewFolder) => {
  return axios.post(`${url}/folder`, { name_folder: nameNewFolder }, {
    headers: {
      Authorization: token(),
    },
  })
}

export const editFolderService = (oldNameFolder, nameNewFolder) => {
  return axios.patch(`${url}/folder`, { name_old: oldNameFolder, name_new: nameNewFolder }, {
    headers: {
      Authorization: token(),
    },
  })
}