import axios from "axios";

const token = () =>  localStorage.getItem('feathers-jwt');
const url = process.env.REACT_APP_API;

export const getItemToGalleryService = (name, urlRef) => {
  return axios.get(`${url}${urlRef}`, {
    headers: {
      Authorization: token(),
    },
  })
}

export const addItemToGalleryService = (name, tab, urlRef) => {
  return axios.patch(`${url}${urlRef}`,
    { [`${name}_id`]: tab }, {
      headers: {
        Authorization: token(),
      }
    })
}

export const delFileWithGalleryService = (name, itemInBase, urlRef) => {
  return axios.patch(`${url}${urlRef}`,
    { [`${name}_id`]: itemInBase }, {
      headers: {
        Authorization: token(),
      }
    })
}