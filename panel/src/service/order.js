import axios from "axios";

const token = () =>  localStorage.getItem('feathers-jwt');
const url = process.env.REACT_APP_API;

export const updateOrder = (data,id,collection) => {
    return axios.patch(`${url}/${collection}/${id}`, data, {
      headers: {
        Authorization: token(),
      },
    });
  }