import React, { useState } from 'react';
import Card from '@material-ui/core/Card';
import { Link } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { resetPwdLong } from './service';
import { domain } from '../../feathersClient';

function RestoryNext(props) {
    const [password, setPassword] = useState('');
    let { hash } = props;
    
    const handleSend = () => {
        resetPwdLong(hash, password).then(() => {
            alert("Hasło zostało zmienione");
            window.location.href = `${domain}/panel/#/login`;
        }).catch((e) => {
            alert("Cos poszło nie tak");
        });
    }
    return (
        <Card className="serviceUser__restory">
            <h3>Podaj nowe hasło do konta.</h3>
            <TextField label="Nowe hasło" type="password" value={password} onChange={(e) => { setPassword(e.target.value) }} />
            <Button variant="contained" className="serviceUser__restory__send" onClick={handleSend}>
                Zmień
                </Button>
            <div className="serviceUser__back">
                <Link exact="true" to={"/login"}  >Cofnij</Link>
            </div>

        </Card>
    )
}

export default RestoryNext;