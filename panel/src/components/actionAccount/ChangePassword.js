import React, { useState, useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { passwordChange } from './service';
import AlertHoc from '../alertHoc';

function ChangePassword() {
    const [email, setEmail] = useState('');
    const [oldPassword, setOldPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [alert, setAlert] = useState([false, "", true]);

    const handleSend = () => {
        passwordChange(email, oldPassword, newPassword).then(() => {
            setAlert([true, "Hasło zostało zmienione", true])
        }).catch((e) => {
            setAlert([true, "Coś poszło nie tak sprawdź dane", false])
        });
    }

    useEffect(() => {
        let timer;
        if (alert[0] === true) {
            timer = setTimeout(() => { setAlert([false, "", true]) }, 2500)
        }
        return () => clearTimeout(timer);
    });
    return (
        <>
            <h3>Zmiana hasła</h3>
            <TextField className="settingsAccount__elem" label="Email" type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
            <TextField className="settingsAccount__elem" label="Stare hasło" type="password" value={oldPassword} onChange={(e) => setOldPassword(e.target.value)} />
            <TextField className="settingsAccount__elem" label="Nowe hasło" type="password" value={newPassword} onChange={(e) => setNewPassword(e.target.value)} />
            <Button variant="contained" className="settingsAccount__elem" onClick={handleSend}>Zmień</Button>
            <AlertHoc alert={alert} />
        </>
    )
}

export default ChangePassword;