import axios from "axios";

const url = process.env.REACT_APP_API;
const token = () => localStorage.getItem('feathers-jwt');


export const verifySignupLong = (hash) => {
    return axios.post(`${url}/authManagement`, {
        action: 'verifySignupLong',
        value: hash,
    })
}

export const sendResetPwd = (email) => {
    return axios.post(`${url}/authManagement`, {
        action: 'sendResetPwd',
        value: { email: email },
    })
}
export const resetPwdLong = (token, password) => {
    return axios.post(`${url}/authManagement`, {
        action: 'resetPwdLong',
        value: {
            token,
            password,
        }
    })
}

export const passwordChange = (email, oldPassword, password) => {
    return axios.post(`${url}/authManagement`, {
        action: 'passwordChange',
        value: {
            user: { email: email },
            oldPassword,
            password,
        }
    }, {
            headers: {
                Authorization: token(),
            }
        }
    )

}

export const identityChange = (email,password,newEmail) => {
    return axios.post(`${url}/authManagement`, {
        action: 'identityChange',
        value: {
            user: { email: email },
            password,
            changes: { email: newEmail }
        }
    }, {
            headers: {
                Authorization: token(),
            }
        }
    )
}

