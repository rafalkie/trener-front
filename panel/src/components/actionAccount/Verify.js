import React, { useEffect } from 'react';
import { verifySignupLong } from './service';
import { domain } from '../../feathersClient';

function Verify(props) {
  const  handleVerify = () => {
        let { hash,type } = props;
        verifySignupLong(hash).then(() => {
            if(type === "change"){
                alert("Twoje dane zostały zmienione");
            }else{
                alert("Twoje konto zostało zweryfikowane");
            }
            window.location.href = `${domain}/panel/#/login`;
        }).catch((e) => {
            alert("Coś poszło, nie tak");
            window.location.href = `${domain}/panel/#/login`;
        });
    }
    useEffect(() => {
        handleVerify();
    });
    return null
};

export default Verify;