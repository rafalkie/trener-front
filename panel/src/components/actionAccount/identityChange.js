import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { identityChange } from './service';
import AlertHoc from './../../components/alertHoc';
import { useEffect } from 'react';
function IdentityChange() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [newEmail, setNewEmail] = useState('');
    const [alert, setAlert] = useState([false, "", true]);


    const handleSend = () => {
        identityChange(email, password, newEmail).then(() => {
            setAlert([true, "Email został zmieniony", true])
        }).catch((e) => {
            setAlert([true, "Coś poszło nie tak sprawdź dane", false])

        });
    }
    useEffect(() => {
        let timer;
        if (alert[0] === true) {
            timer = setTimeout(() => { setAlert([false, "", true]) }, 2500)
        }
        return () => clearTimeout(timer);
    });
    return (
        <>
            <h3>Zmiana emaila</h3>
            <TextField className="settingsAccount__elem" label="Email" type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
            <TextField className="settingsAccount__elem" label="Hasło" type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
            <TextField className="settingsAccount__elem" label="Nowy email" type="email" value={newEmail} onChange={(e) => setNewEmail(e.target.value)} />

            <Button variant="contained" className="settingsAccount__elem" onClick={handleSend}>Zmień</Button>
            <AlertHoc alert={alert} />
        </>
    )
}

export default IdentityChange;
