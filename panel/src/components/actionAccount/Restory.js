import React, { useState } from 'react';
import Card from '@material-ui/core/Card';
import { Link } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { sendResetPwd } from './service';
import { domain } from '../../feathersClient';

function Restory() {
    const [email, setEmail] = useState('');

    const handleSend = () => {
        sendResetPwd(email).then(() => {
            alert("Wysłaliśmy dalsze instrukcje na tego emaila");
            window.location.href = `${domain}/panel/#/login`;
        }).catch((e) => {
            alert("Nieprawidłowy email");
        });
    }
    return (
        <Card className="serviceUser__restory">
            <h3>Odzyskaj hasło do konta.</h3>
            <p>Podaj email konta wyślemy dalsze instrukcje</p>
            <TextField label="Email" type="email" value={email} onChange={(e) => (setEmail(e.target.value))} />
            <Button variant="contained" className="serviceUser__restory__send" onClick={handleSend}>
                Wyślij
                </Button>
            <div className="serviceUser__back">
                <Link exact="true" to={"/login"}  >Cofnij</Link>
            </div>
        </Card>
    )
}
export default Restory;