import React, { useState,useEffect  } from 'react'

function AlertHoc(props) {

    const [alert, setAlert] = useState(props.alert);
    useEffect(() => {
        setAlert(props.alert);
      });
      
        let classAlert = "";
        if (alert[0] === true) {
            classAlert = "alert enable";
            if (alert[2] === true) {
                classAlert = "alert enable success"
            } else {
                classAlert = "alert enable error"
            }
        } else {
            classAlert = "alert"
        }
        return (
            <div className={classAlert}>
                <p>{alert[1]}</p>
            </div>
        )
    }

export default AlertHoc;
