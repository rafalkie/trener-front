import React, { useState } from 'react'

function ImageLoader(props) {
	const [loading,setLoading] = useState(false);

	const {item, alt, width = "", height = ""} = props;
  return (
    <>
    {!loading && "Trwa ładowanie ..."}
    <img
      
      style={{display: loading ? "block" : " none"}}
      width={width}
      height={height}
      onLoad={()=>setLoading(true)}
      src={`${process.env.REACT_APP_API}/uploads/${item && item.folder}/${item && item.name}`}
      alt={alt}
    />
    </>
  );
}

export default ImageLoader
