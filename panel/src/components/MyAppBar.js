// in src/MyAppBar.js
import React from 'react';
import { AppBar } from 'react-admin';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import MyUserMenu from './MyUserMenu'
import Logo from './layout/Logo';

const styles = {
    title: {
        flex: 1,
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
    },
    spacer: {
        flex: 1,
    },
};

const MyAppBar = withStyles(styles)(({ classes, ...props }) => (
    <AppBar {...props} userMenu={<MyUserMenu/>}>
        <Typography
            // variant="title"
            color="inherit"
            className={classes.title}
            id="react-admin-title"
        />
        <Logo />
    </AppBar>
));

export default MyAppBar;