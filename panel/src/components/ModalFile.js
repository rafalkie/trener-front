import React, { useState } from "react";
import UploadFile from "./../plugin/File/component/uploadFile";
import File from "./../plugin/File/component/file";
import Alert from "./alert";
import FileItem from "./../plugin/File/component/FileItem";
import Modal from "react-responsive-modal";

function ModalFile(props) {
  const { name, type, title, task } = props.file;
  const [visible, setVisible] = useState(false);
  return (
    <>
      <FileItem
        setVisible={() => setVisible(true)}
        title={title}
        {...props}
        name={name}
        type={type}
        task={task}
        changePhoto={props.changePhoto}
      />
      <Modal
        open={visible}
        onClose={() => setVisible(false)}
        center
        classNames={{ modal: "modalFile" }}
      >
        <div>
          <UploadFile name={name} />
          <File task={task} name={name} {...props} type={type} />
          <Alert />
        </div>
      </Modal>
    </>
  );
}

export default ModalFile;
