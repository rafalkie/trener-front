export const imgUrl = `${process.env.REACT_APP_API}/uploads/`;
export const appUrlImg = `${process.env.REACT_APP_DOMAIN}/panel`;
export const isEmptyObject = (data) =>(
   Object.keys(data) === 0
)
export const isEmptyTab= (data) =>(
  data.length === 0
)
export const exist = (data) =>(
  data !== undefined
)
export const cardStyle = {
	width: 300,
	minHeight: 300,
	margin: "0.5em",
	display: "inline-block",
	verticalAlign: "top"
};

