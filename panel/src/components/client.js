import React, { Component } from 'react';
import { Login, LoginForm } from 'react-admin';
import { Link } from 'react-router-dom';
import polishMessages from 'ra-language-polish';
import Verify from './actionAccount/Verify';
import Restory from './actionAccount/Restory';
import RestoryNext from './actionAccount/RestoryNext';
import polyglotI18nProvider from 'ra-i18n-polyglot';

export class MyLoginPage extends Component {
  render() {
    let { search } = this.props.location;
    let pathIndex = search.indexOf('$');
    let path = search.slice(0,pathIndex-1);
    let hash = search.slice(pathIndex+7);
    return (
      <div className="serviceUser">
        {(search === '' && hash == '') &&
          <div className="serviceUser__login">
            <Login backgroundImage=""/>
            <Link exact="true" className="link-restory" to={"/login/?restored"} >Przywróc hasło</Link>

                        </div>
        }

        {( path === '?verifyChanges') && <Verify hash={hash} type={"change"} />}
        {(path === '?verify') && <Verify hash={hash} type={"verify"}  />}
        {path === '?reset' && <RestoryNext hash={hash} />}
        {search === '?restored' && <Restory hash={hash} />}


      </div>
    )
  }
};
export const messages = {
  pl: polishMessages,
}
export const i18nProvider = polyglotI18nProvider(() => polishMessages, 'pl'); 

