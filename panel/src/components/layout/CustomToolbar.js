import React from "react";
import { SaveButton, Toolbar } from "react-admin";
import { useDispatch } from "react-redux";
import { getSave } from "../../plugin/File/action";

function CustomToolbar(props) {
	const dispatch = useDispatch();
	const get_Save = (save) => dispatch(getSave(save));
	return (
		<Toolbar {...props}>
			<SaveButton onClick={() => get_Save(true)} />
		</Toolbar>
	);
}

export default CustomToolbar;
