import React from "react";
import Button from "@material-ui/core/Button";
import { CreateButton, ExportButton, RefreshButton } from "react-admin";
import Toolbar from "@material-ui/core/Toolbar";

const PostActions = ({
  basePath,
  currentSort,
  displayedFilters,
  exporter,
  filters,
  filterValues,
  onUnselectItems,
  resource,
  selectedIds,
  showFilter,
  total,
  order,
  visible
}) => (
  <Toolbar>
    {visible && (
      <>
        {filters &&
          React.cloneElement(filters, {
            resource,
            showFilter,
            displayedFilters,
            filterValues,
            context: "button"
          })}
        <CreateButton basePath={basePath} />
      </>
    )}
    <Button color="primary" onClick={order}>
      {visible ? "Kolejność" : "Powrót"}
    </Button>
  </Toolbar>
);

export default PostActions;
