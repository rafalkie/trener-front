// in src/Menu.js
import React, { useState } from "react";
import { connect, useSelector } from "react-redux";
import { MenuItemLink, getResources, usePermissions } from "react-admin";
import { withRouter } from "react-router-dom";
import OtherIcon from "@material-ui/icons/Bookmark";
import CommentIcon from "@material-ui/icons/Comment";
import FileCopy from "@material-ui/icons/FileCopy";
import GalleryIcon from "@material-ui/icons/PhotoLibrary";
import LiveHelp from "@material-ui/icons/LiveHelp";
import Accessibility from "@material-ui/icons/Accessibility";
import Forward10 from "@material-ui/icons/Forward10";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Pages from "@material-ui/icons/Pages";
import Forum from "@material-ui/icons/Forum";
import ContactMail from "@material-ui/icons/ContactMail";
import SubMenu from "./SubMenu";

const Menu = ({ dense, onMenuClick, logout }) => {
  const [state, setState] = useState({
    menuCatalog: false,
    menuSales: false,
    menuCustomers: false
  });
  const open = useSelector((state: AppState) => state.admin.ui.sidebarOpen);
  useSelector((state: AppState) => state.theme); // force rerender on theme change

  const handleToggle = (menu: MenuName) => {
    setState(state => ({ ...state, [menu]: !state[menu] }));
  };
  const { permissions } = usePermissions();
  return (
    <div>
      <MenuItemLink to="/news" primaryText="Aktualności" leftIcon={<Forum />} />
      <MenuItemLink to="/page" primaryText="Strony" leftIcon={<Pages />} />

      <SubMenu
        handleToggle={() => handleToggle("menuSales")}
        isOpen={state.menuSales}
        sidebarIsOpen={open}
        icon={<LocalOffer />}
        name="Oferty"
        dense={dense}
      >
        <MenuItemLink
          to={"/offer"}
          sidebarIsOpen={open}
          primaryText="Oferta"
          leftIcon={<LocalOffer />}
          dense={dense}
        />

        <MenuItemLink
          to="/forms"
          sidebarIsOpen={open}
          primaryText="Ankiety"
          leftIcon={<ContactMail />}
          dense={dense}
        />
      </SubMenu>

      <MenuItemLink
        to="/achievements"
        primaryText="Osiągniecia"
        leftIcon={<Forward10 />}
        dense={dense}
      />

      <MenuItemLink
        to="/support"
        primaryText="W czym pomagam"
        leftIcon={<Accessibility />}
        dense={dense}
      />
      <MenuItemLink
        to="/galleries/"
        primaryText="Galeria"
        leftIcon={<GalleryIcon />}
        dense={dense}
      />
      <MenuItemLink
        to="/references"
        primaryText="Referencje"
        leftIcon={<CommentIcon />}
        dense={dense}
      />
      <MenuItemLink
        to="/faq"
        primaryText="FAQ"
        leftIcon={<LiveHelp />}
        dense={dense}
      />

      <MenuItemLink
        to="/others/5deb86a5df9ab434407539f5"
        primaryText="Pozostałe"
        leftIcon={<OtherIcon />}
        dense={dense}
      />

      <MenuItemLink
        to="/files/"
        primaryText="Pliki"
        leftIcon={<FileCopy />}
        dense={dense}
      />
      {permissions === "admin" && (
        <MenuItemLink to="/users/" primaryText="Użytkownicy" dense={dense} />
      )}
    </div>
  );
};

const mapStateToProps = state => ({
  resources: getResources(state)
});

export default withRouter(connect(mapStateToProps)(Menu));
