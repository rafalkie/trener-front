import React, { Component } from 'react'
import { connect } from 'react-redux';
import actions from './../plugin/File/action';

class Alert extends Component {

    componentDidUpdate(prevProps, prevState) {
        if (this.props.list.alert[0] === true) {
            setTimeout(() => { this.props.getAlert(false, "", true) }, 2500)
        }
    }

    render() {
        let { alert } = this.props.list;
        let classAlert = "";
        if (alert[0] === true) {
            classAlert = "alert enable";
            if (alert[2] === true) {
                classAlert = "alert enable success"
            } else {
                classAlert = "alert enable error"
            }
        } else {
            classAlert = "alert"
        }
        return (
            <div className={classAlert}>
                <p>{alert[1]}</p>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    list: state.files,
})

const mapDispatchToProps = (dispatch) => {
    return {
        getAlert: (display, message, status) => dispatch(actions.getAlert(display, message, status)),
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Alert);
