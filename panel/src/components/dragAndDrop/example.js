import React, { useState, useCallback } from "react";
import Card from "./Card";
import update from "immutability-helper";
import { useEffect } from "react";
import { updateOrder } from "../../service/order";
import {
  Button,
  useRefresh,
} from 'react-admin';


const style = {
  width: 400,
  margin:"auto"
};
function Example(props) {
  const [cards, setCards] = useState();
  const [change, setChange] = useState(false);
  const refresh = useRefresh();
  useEffect(() => {
    if (props.data !== undefined) {
      setCards(props.data);
    }
  }, []);

  const onSave = async () => {
    if (change) {
      await cards.map(async(item, index)  => {
        await updateOrder({ position: index }, item._id, props.basePath);
      });
    }
    await setChange(false);
    await props.setVisible();
    refresh();
  };
  const moveCard = useCallback(
    (dragIndex, hoverIndex) => {
      const dragCard = cards[dragIndex];
      setCards(
        update(cards, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, dragCard]
          ]
        })
      );
      if (!change) setChange(true);
    },
    [cards]
  );

  const renderCard = (card, index) => {
    return (
      <Card
        key={card.id}
        index={index}
        id={card.id}
        text={card.text}
        moveCard={moveCard}
      />
    );
  };
  return (
    <>
      <div style={style}>
        {cards && cards.map((card, i) => renderCard(card, i))}
      </div>
      <Button
        style={{ position: "absolute", bottom: "10px",zIndex :"2" }}
        onClick={onSave}
        label="Zapisz"
      />

    </>
  );
}
export default Example;
