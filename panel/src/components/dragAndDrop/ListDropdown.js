import React from "react";
import DragAndDrop from ".";

const ListDropDown = ({ ids, data, basePath, setVisible, name,optionalName }) => {
    let result = [];

    for(let i = 0;i<ids.length;i++){
        let item = ids[i];
        result[data[item].position] ={
            id: data[item].position,
            _id: data[item]._id,
            text: data[item][name] !== undefined ? data[item][name] : data[item][optionalName]
          }
    }
  return (
    <div style={{ margin: "1em" }}>
      {result.length > 0 ? (
        <DragAndDrop
          data={result.length > 0 ? result : []}
          setVisible={setVisible}
          basePath={basePath.slice(1)}
        />
      ) : null}
    </div>
  );
};

export default ListDropDown;
